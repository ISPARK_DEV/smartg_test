Attribute VB_Name = "CarLine"
Const KEYEVENTF_DOWN = &H0
Const KEYEVENTF_KEYUP = &H13
Public Declare Sub keybd_event Lib "user32" (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)
'--------------------------------------------------------------------------------------------------- View Value
Public Selection_View_Object
'--------------------------------------------------------------------------------------------------- View Size
Public View_Size(4), Xmin, Xmax, Ymin, Ymax
Public Drawing_Name_Len, note_detail_sheet
Public Symbol_Type_Value(50), Symbol_Type_Value_Number
'--------------------------------------------------------------------------------------------------- Position Center
Public CEILING_XCenter(2) As Integer
Public CEILING_YCenter(2) As Integer
'--------------------------------------------------------------------------------------------------- 원크기
Public Circle_Radius As Double
'--------------------------------------------------------------------------------------------------- Text_Size
Public Position_Text_Size As Double, Number_Text_Size As Double
'--------------------------------------------------------------------------------------------------- 간격
Public CarLine_Space As Integer
'--------------------------------------------------------------------------------------------------- Drawing_Scale
Public Drawing_Scale As Double
'--------------------------------------------------------------------------------------------------- CarLine
Public WindowLocation(1), HardCodedPoint, Status, XCenter(2), YCenter(2), InputObjectType(0), TempCircleHasBeenCreatedAtLeastOnce, ExistingPoint
Public ObjectSelected, Point2D(2)
Public X_Transfer_Value As Integer, Y_Transfer_Value As Integer
'--------------------------------------------------------------------------------------------------- Axis
Public Axis_object
'--------------------------------------------------------------------------------------------------- Factory2D
Public Selection_View_Object_Factory2D
'--------------------------------------------------------------------------------------------------- View 초기위치
Public Selection_View_Object_Origin_X, Selection_View_Object_Origin_Y
'--------------------------------------------------------------------------------------------------- Y_Max_Margin
Public Y_Max_Margin, Rotate_X_Margin
Public X_Plus_Margin, X_Minus_Margin, Y_Plus_Margin, Y_Minus_Margin
'--------------------------------------------------------------------------------------------------- CarLine Roate
Public CarLine_Rotate_Number
Public View_Rotate_X_Minus, View_Rotate_X_Plus, View_Rotate_Y_Minus, View_Rotate_Y_Plus
Public View_Rotate_X_Start_Point(100, 2), View_Rotate_X_End_Point(100, 2), View_Rotate_Y_Start_Point(100, 2), View_Rotate_Y_End_Point(100, 2)
'--------------------------------------------------------------------------------------------------- Title Box Position
Public Title_Box_Left_Position_Value, Title_Box_Upper_Position_Value
Public Create_View_Size(3, 4)
Public CarLine_Axis_object, CarLine_Components(11), EndPoints_Value(4)
Public CarLine_Rotate_X_Value As Double, CarLine_Rotate_Y_Value As Double, Car_Line_horizontal_Point(4) As Double
Public Rotate_Angle
Public CarLine_Margin_Value_X, CarLine_Margin_Value_Y
Public CarLine_Rotate_Point_Count01, CarLine_Rotate_Point_Count02
Public Horizental_Axis_Value, Horizental_Marking_Value, Vertical_Axis_Value, Vertical_Marking_Value
'---------------------------------------------------------------------------------------------------
'  CarLine_Combo_Setting &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub CarLine_Combo_Setting()
'--------------------------------------------------------------------------------------------------- Car_Line_Circle_Size_Text
CAR_Line_Form.Car_Line_Text_Size_Text.AddItem "2.5"
CAR_Line_Form.Car_Line_Text_Size_Text.AddItem "5"
CAR_Line_Form.Car_Line_Text_Size_Text.AddItem "7.5"
CAR_Line_Form.Car_Line_Text_Size_Text.AddItem "10"
'--------------------------------------------------------------------------------------------------- Car_Line_Circle_Size_Text
CAR_Line_Form.Car_Line_Circle_Size_Text.AddItem "5"
CAR_Line_Form.Car_Line_Circle_Size_Text.AddItem "10"
CAR_Line_Form.Car_Line_Circle_Size_Text.AddItem "15"
CAR_Line_Form.Car_Line_Circle_Size_Text.AddItem "20"
'--------------------------------------------------------------------------------------------------- Car_Line_Position_Combo
CAR_Line_Form.Car_Line_Position_Combo.AddItem "T,L,H"
CAR_Line_Form.Car_Line_Position_Combo.AddItem "X,Y,Z"
CAR_Line_Form.Car_Line_Position_Combo.AddItem "TL,BL,WL"
'--------------------------------------------------------------------------------------------------- Car_Line_Space_Combo
CAR_Line_Form.Car_Line_Space_Combo.AddItem "100"
CAR_Line_Form.Car_Line_Space_Combo.AddItem "200"
CAR_Line_Form.Car_Line_Space_Combo.AddItem "300"
CAR_Line_Form.Car_Line_Space_Combo.AddItem "500"
CAR_Line_Form.Car_Line_Space_Combo.AddItem "1000"
CAR_Line_Form.Car_Line_Space_Combo.AddItem "Customize"
'--------------------------------------------------------------------------------------------------- Car_LineTarg_Combo
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Top-Left"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Top-Right"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Bottom-Left"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Bottom-Right"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "All"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Left"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Right"
CAR_Line_Form.Car_LineTarg_Combo.AddItem "Top"
End Sub
'---------------------------------------------------------------------------------------------------
'  CATIA_CarLine_Delete &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub CATIA_CarLine_Delete()
    Call CATIA_Basic_Drawing
    Call Activate_View_CarLine_Delete
End Sub
'---------------------------------------------------------------------------------------------------
'  CATIA_CarLine_Create &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub CATIA_CarLine_Create(Selection_View_Value, CarLine_Rotate_Value)
'--------------------------------------------------------------------------------------------------- Drawing Axis Direction Define
Call Drawing_Axis_Direction_Define(Selection_View_Value)
If CarLine_Rotate_Value = 3 Then
    '--------------------------------------------------------------------------------------------------- Car_Line 간격
    CarLine_Space = CAR_Line_Form.Car_Line_Space_Combo.Text
    '--------------------------------------------------------------------------------------------------- Selection_View_Object_Factory2D
    Set Selection_View_Object_Factory2D = Selection_View_Value.Factory2D
    Set visProperties1 = Drawingselection.VisProperties
    '--------------------------------------------------------------------------------------------------- Indicate_Point 생성
    For i = 1 To 2 Step 1
        Status = CATIA.ActiveDocument.Indicate2D("click to define the circle center", WindowLocation)
        If (Status = "Cancel" Or Status = "Undo" Or Status = "Redo") Then Exit Sub
            XCenter(i) = WindowLocation(0)
            YCenter(i) = WindowLocation(1)
            InputObjectType(0) = "Point2D"
            Status = "MouseMove": TempCircleHasBeenCreatedAtLeastOnce = 0
            Status = Selection.IndicateOrSelectElement2D("select a point or click to locate the circle radius point", InputObjectType, False, False, True, ObjectSelected, WindowLocation)
        If (TempCircleHasBeenCreatedAtLeastOnce) Then
            Selection.Add Point2D
            Selection.Delete
        End If
        Set Point2D(i) = Selection_View_Object_Factory2D.CreatePoint(XCenter(i), YCenter(i))
        TempCircleHasBeenCreatedAtLeastOnce = 1
        Status = Selection.IndicateOrSelectElement2D("select a point or click to locate the circle radius point", InputObjectType, False, False, True, ObjectSelected, WindowLocation)
        '--------------------------------------------------------------------------------------------------- Point 위치값 변경
        If i = 1 Then
            CEILING_XCenter(i) = Int(XCenter(i) / CarLine_Space)
            CEILING_YCenter(i) = Int(YCenter(i) / CarLine_Space) + 1
        ElseIf i = 2 Then
            CEILING_XCenter(i) = Int(XCenter(i) / CarLine_Space) + 1
            CEILING_YCenter(i) = Int(YCenter(i) / CarLine_Space)
        End If
    Next i
    CAR_Line_Form.Message_Label = "CarLine 생성 중..."
    '--------------------------------------------------------------------------------------------------- Car_Line_Circle_Size & Position_Text_Size
    Circle_Radius = CDbl(CAR_Line_Form.Car_Line_Circle_Size_Text.Text)
    Position_Text_Size = CDbl(CAR_Line_Form.Car_Line_Text_Size_Text.Text)
    Number_Text_Size = CDbl(CAR_Line_Form.Car_Line_Text_Size_Text.Text)
    If CAR_Line_Form.CarLine_Option1.Value = True Then
        CarLine_Rotate_Point_Count01 = 1
        '--------------------------------------------------------------------------------------------------- Line 생성(가로)
        For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
            Set Car_Line_horizontal = Selection_View_Object_Factory2D.CreateLine(CEILING_XCenter(1) * CarLine_Space, i, CEILING_XCenter(2) * CarLine_Space, i)
            Car_Line_horizontal.Name = "CATIA_CarLine_Horizontal_" & i
            '--------------------------------------------------------------------------------------------------- 두께 변경
            Drawingselection.Clear
            Drawingselection.Add Car_Line_horizontal
            visProperties1.SetRealWidth 1, 1
            visProperties1.SetRealLineType 1, 1
            Drawingselection.Clear
            '--------------------------------------------------------------------------------------------------- Line 좌표값 입력 ( Carline Text 생성시 필요 )
            View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01, 1) = CEILING_XCenter(1) * CarLine_Space
            View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01, 2) = i
            View_Rotate_X_End_Point(CarLine_Rotate_Point_Count01, 1) = CEILING_XCenter(2) * CarLine_Space
            View_Rotate_X_End_Point(CarLine_Rotate_Point_Count01, 2) = i
            CarLine_Rotate_Point_Count01 = CarLine_Rotate_Point_Count01 + 1
        Next i
        CarLine_Rotate_Point_Count02 = 1
        For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
            Set Car_Line_Vertical = Selection_View_Object_Factory2D.CreateLine(i, CEILING_YCenter(1) * CarLine_Space, i, CEILING_YCenter(2) * CarLine_Space)
            Car_Line_Vertical.Name = "CATIA_CarLine_Vertical_" & i
            '--------------------------------------------------------------------------------------------------- 두께 변경
            Drawingselection.Clear
            Drawingselection.Add Car_Line_Vertical
            visProperties1.SetRealWidth 1, 1
            visProperties1.SetRealLineType 1, 1
            Drawingselection.Clear
            '--------------------------------------------------------------------------------------------------- Line 좌표값 입력 ( Carline Text 생성시 필요 )
            View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02, 1) = i
            View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02, 2) = CEILING_YCenter(1) * CarLine_Space
            View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count02, 1) = i
            View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count02, 2) = CEILING_YCenter(2) * CarLine_Space
            CarLine_Rotate_Point_Count02 = CarLine_Rotate_Point_Count02 + 1
        Next i
        Call Drawing_Axis_Direction_Define(Selection_View_Value)
        '--------------------------------------------------------------------------------------------------- Targ Position
        If CAR_Line_Form.Car_LineTarg_Combo.Text = "Top-Left" Then
           Call targ_Position_Top_Option
           Call targ_Position_Left_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Top-Right" Then
           Call targ_Position_Top_Option
           Call targ_Position_Right_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Bottom-Left" Then
           Call targ_Position_Bottom_Option
           Call targ_Position_Left_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Bottom-Right" Then
           Call targ_Position_Bottom_Option
           Call targ_Position_Right_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "All" Then
           Call targ_Position_Top_Option
           Call targ_Position_Bottom_Option
           Call targ_Position_Left_Option
           Call targ_Position_Right_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Left" Then
            Call targ_Position_Left_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Right" Then
            Call targ_Position_Right_Option
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Top" Then
            Call targ_Position_Top_Option
        End If
    Else
        '--------------------------------------------------------------------------------------------------- View 정보 가져오기
        CarLine_Rotate_Number = 1
        Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
        If X1 = 0 Then
            View_Rotate_X_Minus = Y1
            View_Rotate_X_Plus = Y2
            View_Rotate_Y_Minus = Z1
            View_Rotate_Y_Plus = Z2
            T_Value = 0
        ElseIf Y1 = 0 Then
            View_Rotate_X_Minus = X1
            View_Rotate_X_Plus = X2
            View_Rotate_Y_Minus = Z1
            View_Rotate_Y_Plus = Z2
            L_Value = 0
        Else
            View_Rotate_X_Minus = X1
            View_Rotate_X_Plus = X2
            View_Rotate_Y_Minus = Y1
            View_Rotate_Y_Plus = Y2
            H_Value = 0
        End If
        CarLine_Rotate_Point_Count01 = 1
        Cos_Value = Y1
        Sin_Value = Y2
        '--------------------------------------------------------------------------------------------------- 이동 거리 계산 ( 회전 각도로 인한 오차 계산)
        Call CarLine_Point_Rotate_Function(CEILING_XCenter(1) * CarLine_Space, (CEILING_YCenter(1) - 1) * CarLine_Space, 1)
        CarLine_Margin_Value_X = Abs(CarLine_Rotate_X_Value - (CEILING_XCenter(1) * CarLine_Space)) / CarLine_Space
        CarLine_Margin_Value_Y = Abs(CarLine_Rotate_Y_Value - (CEILING_YCenter(1) - 1) * CarLine_Space) / CarLine_Space
        If Rotate_Angle < 0 Then
            CarLine_Margin_Value_X = Int(CarLine_Margin_Value_X) * CarLine_Space * -1
            CarLine_Margin_Value_Y = Int(CarLine_Margin_Value_Y) * CarLine_Space * -1
        Else
            CarLine_Margin_Value_X = Int(CarLine_Margin_Value_X) * CarLine_Space
            CarLine_Margin_Value_Y = Int(CarLine_Margin_Value_Y) * CarLine_Space
        End If
        '--------------------------------------------------------------------------------------------------- Line 생성(가로)
        For i = (((CEILING_YCenter(1) - 1) * CarLine_Space)) To (((CEILING_YCenter(2) + 1) * CarLine_Space)) Step -CarLine_Space
            '--------------------------------------------------------------------------------------------------- 좌,상 포인트 계산
            Call CarLine_Point_Rotate_Function(CEILING_XCenter(1) * CarLine_Space + CarLine_Margin_Value_X, i + CarLine_Margin_Value_Y, 1)
            Car_Line_horizontal_Point(1) = CarLine_Rotate_X_Value
            Car_Line_horizontal_Point(2) = CarLine_Rotate_Y_Value
            '--------------------------------------------------------------------------------------------------- 우,상 포인트 계산
            Call CarLine_Point_Rotate_Function(CEILING_XCenter(2) * CarLine_Space + CarLine_Margin_Value_X, i + CarLine_Margin_Value_Y, 1)
            Car_Line_horizontal_Point(3) = CarLine_Rotate_X_Value
            Car_Line_horizontal_Point(4) = CarLine_Rotate_Y_Value
            '--------------------------------------------------------------------------------------------------- Line 생성
            Set Car_Line_horizontal = Selection_View_Object_Factory2D.CreateLine(Car_Line_horizontal_Point(1), Car_Line_horizontal_Point(2), Car_Line_horizontal_Point(3), Car_Line_horizontal_Point(4))
            Car_Line_horizontal.Name = "CATIA_CarLine_Horizontal_" & i
            '--------------------------------------------------------------------------------------------------- 두께 변경
            Drawingselection.Clear
            Drawingselection.Add Car_Line_horizontal
            visProperties1.SetRealWidth 1, 1
            visProperties1.SetRealLineType 1, 1
            Drawingselection.Clear
            Car_Line_horizontal.GetEndPoints EndPoints_Value
            '--------------------------------------------------------------------------------------------------- Line 좌표값 입력 ( Carline Text 생성시 필요 )
            View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01, 1) = EndPoints_Value(0)
            View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01, 2) = EndPoints_Value(1)
            View_Rotate_X_End_Point(CarLine_Rotate_Point_Count01, 1) = EndPoints_Value(2)
            View_Rotate_X_End_Point(CarLine_Rotate_Point_Count01, 2) = EndPoints_Value(3)
            CarLine_Rotate_Point_Count01 = CarLine_Rotate_Point_Count01 + 1
        Next i
        CarLine_Rotate_Point_Count02 = 1
        Cos_Value = Z1
        Sin_Value = Z2
        '--------------------------------------------------------------------------------------------------- Line 생성 (세로)
        For i = (((CEILING_XCenter(1) + 1) * CarLine_Space)) To (((CEILING_XCenter(2) - 1) * CarLine_Space)) Step CarLine_Space
            '--------------------------------------------------------------------------------------------------- 좌,상 포인트 계산
            Call CarLine_Point_Rotate_Function(i + CarLine_Margin_Value_X, (CEILING_YCenter(1)) * CarLine_Space + CarLine_Margin_Value_Y, 1)
            Car_Line_horizontal_Point(1) = CarLine_Rotate_X_Value
            Car_Line_horizontal_Point(2) = CarLine_Rotate_Y_Value
            '--------------------------------------------------------------------------------------------------- 좌,하 포인트 계산
            Call CarLine_Point_Rotate_Function(i + CarLine_Margin_Value_X, (CEILING_YCenter(2)) * CarLine_Space + CarLine_Margin_Value_Y, 1)
            Car_Line_horizontal_Point(3) = CarLine_Rotate_X_Value
            Car_Line_horizontal_Point(4) = CarLine_Rotate_Y_Value
            Set Car_Line_Vertical = Selection_View_Object_Factory2D.CreateLine(Car_Line_horizontal_Point(1), Car_Line_horizontal_Point(2), Car_Line_horizontal_Point(3), Car_Line_horizontal_Point(4))
            Car_Line_Vertical.Name = "CATIA_CarLine_Vertical_" & i
            X_Move_Number = X_Move_Number + 1
            '--------------------------------------------------------------------------------------------------- 두께 변경
            Drawingselection.Clear
            Drawingselection.Add Car_Line_Vertical
            visProperties1.SetRealWidth 1, 1
            visProperties1.SetRealLineType 1, 1
            Drawingselection.Clear
            Car_Line_Vertical.GetEndPoints EndPoints_Value
            '--------------------------------------------------------------------------------------------------- Line 좌표값 입력 ( Carline Text 생성시 필요 )
            View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02, 1) = EndPoints_Value(0)
            View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02, 2) = EndPoints_Value(1)
            View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count02, 1) = EndPoints_Value(2)
            View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count02, 2) = EndPoints_Value(3)
            CarLine_Rotate_Point_Count02 = CarLine_Rotate_Point_Count02 + 1
        Next i
        '--------------------------------------------------------------------------------------------------- Targ Position
        If CAR_Line_Form.Car_LineTarg_Combo.Text = "Top-Left" Then
           Call targ_Position_Top_Ref
           Call targ_Position_Left_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Top-Right" Then
           Call targ_Position_Top_Ref
           Call targ_Position_Right_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Bottom-Left" Then
           Call targ_Position_Bottom_Ref
           Call targ_Position_Left_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Bottom-Right" Then
           Call targ_Position_Bottom_Ref
           Call targ_Position_Right_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "All" Then
           Call targ_Position_Top_Ref
           Call targ_Position_Bottom_Ref
           Call targ_Position_Left_Ref
           Call targ_Position_Right_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Left" Then
            Call targ_Position_Left_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Right" Then
            Call targ_Position_Right_Ref
        ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Top" Then
            Call targ_Position_Top_Ref
        End If
        '--------------------------------------------------------------------------------------------------- Car_Line X,Y 생성 ( Reference CarLine 생성할때... )
        If CarLine_Rotate_Number = 1 Then
            Call REF_Center_Line_Create(Selection_View_Object)
        End If
    End If
    Drawingselection.Add (Point2D(1))
    Drawingselection.Add (Point2D(2))
    Drawingselection.Delete
Else
    Call CATIA_Basic_Drawing
    Selection_View_Value.Activate
    '--------------------------------------------------------------------------------------------------- Selection_View_Object_Factory2D
    Set Selection_View_Object_Factory2D = Selection_View_Value.Factory2D
    Set visProperties1 = Drawingselection.VisProperties
    '--------------------------------------------------------------------------------------------------- Point 좌표 찾기
    Dim Selection_Point_Value(4)
    Selection_Margin = 20
    Selection_View_Value.Size View_Size_Check
    Create_View_Size(1, 1) = View_Size_Check(0) - Selection_View_Value.xAxisData - Selection_Margin
    Create_View_Size(1, 2) = View_Size_Check(1) - Selection_View_Value.xAxisData + Selection_Margin
    Create_View_Size(1, 3) = View_Size_Check(2) - Selection_View_Value.yAxisData - Selection_Margin
    Create_View_Size(1, 4) = View_Size_Check(3) - Selection_View_Value.yAxisData + Selection_Margin
    CEILING_XCenter(1) = Int(Create_View_Size(1, 1) / CarLine_Space)
    CEILING_YCenter(1) = Int(Create_View_Size(1, 4) / CarLine_Space) + 1
    CEILING_XCenter(2) = Int(Create_View_Size(1, 2) / CarLine_Space) + 1
    CEILING_YCenter(2) = Int(Create_View_Size(1, 3) / CarLine_Space)
    '--------------------------------------------------------------------------------------------------- Car_Line_Circle_Size & Position_Text_Size
    Circle_Radius = CDbl(CAR_Line_Form.Car_Line_Circle_Size_Text.Text)
    Position_Text_Size = CDbl(CAR_Line_Form.Car_Line_Text_Size_Text.Text)
    Number_Text_Size = CDbl(CAR_Line_Form.Car_Line_Text_Size_Text.Text)
     '--------------------------------------------------------------------------------------------------- Car_Line 간격
    CarLine_Space = CAR_Line_Form.Car_Line_Space_Combo.Text
    '--------------------------------------------------------------------------------------------------- Line 생성(가로)
    CarLine_Rotate_Point_Count01 = 1
    For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
        Set Car_Line_horizontal = Selection_View_Object_Factory2D.CreateLine(CEILING_XCenter(1) * CarLine_Space, i, CEILING_XCenter(2) * CarLine_Space, i)
        Car_Line_horizontal.Name = "CATIA_CarLine_Horizontal_" & i
        '--------------------------------------------------------------------------------------------------- 두께 변경
        Drawingselection.Clear
        Drawingselection.Add Car_Line_horizontal
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
        '--------------------------------------------------------------------------------------------------- Line 좌표값 입력 ( Carline Text 생성시 필요 )
        View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01, 1) = CEILING_XCenter(1) * CarLine_Space
        View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01, 2) = i
        View_Rotate_X_End_Point(CarLine_Rotate_Point_Count01, 1) = CEILING_XCenter(2) * CarLine_Space
        View_Rotate_X_End_Point(CarLine_Rotate_Point_Count01, 2) = i
        CarLine_Rotate_Point_Count01 = CarLine_Rotate_Point_Count01 + 1
    Next i
    CarLine_Rotate_Point_Count02 = 1
    For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
        Set Car_Line_Vertical = Selection_View_Object_Factory2D.CreateLine(i, CEILING_YCenter(1) * CarLine_Space, i, CEILING_YCenter(2) * CarLine_Space)
        Car_Line_Vertical.Name = "CATIA_CarLine_Vertical_" & i
        '--------------------------------------------------------------------------------------------------- 두께 변경
        Drawingselection.Clear
        Drawingselection.Add Car_Line_Vertical
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
        '--------------------------------------------------------------------------------------------------- Line 좌표값 입력 ( Carline Text 생성시 필요 )
        View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02, 1) = i
        View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02, 2) = CEILING_YCenter(1) * CarLine_Space
        View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count02, 1) = i
        View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count02, 2) = CEILING_YCenter(2) * CarLine_Space
        CarLine_Rotate_Point_Count02 = CarLine_Rotate_Point_Count02 + 1
    Next i
    Call Drawing_Axis_Direction_Define(Selection_View_Value)
    '--------------------------------------------------------------------------------------------------- Targ Position
    If CAR_Line_Form.Car_LineTarg_Combo.Text = "Top-Left" Then
       Call targ_Position_Top_Option
       Call targ_Position_Left_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Top-Right" Then
       Call targ_Position_Top_Option
       Call targ_Position_Right_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Bottom-Left" Then
       Call targ_Position_Bottom_Option
       Call targ_Position_Left_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Bottom-Right" Then
       Call targ_Position_Bottom_Option
       Call targ_Position_Right_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "All" Then
       Call targ_Position_Top_Option
       Call targ_Position_Bottom_Option
       Call targ_Position_Left_Option
       Call targ_Position_Right_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Left" Then
        Call targ_Position_Left_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Right" Then
        Call targ_Position_Right_Option
    ElseIf CAR_Line_Form.Car_LineTarg_Combo.Text = "Top" Then
        Call targ_Position_Top_Option
    End If
End If
End Sub
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Top &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub targ_Position_Top()
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
For i = ((CEILING_XCenter(1) + Y_Plus_Margin) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
    '--------------------------------------------------------------------------------------------------- Circle 생성 (가로)
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus)
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(i, CEILING_YCenter(2) * CarLine_Space + Circle_Radius + Y_Minus_Margin, Circle_Radius)
        End If
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (가로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine((i - Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) + Circle_Radius + Y_Minus_Margin, (i + Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) + Circle_Radius + Y_Minus_Margin)
        End If
        circle_horizontal_Line.Name = "CATIA_CarLine_Circle_Line"
'        If CarLine_Rotate_Number = 1 Then
'            circle_horizontal_Line.SetData (i - (X_Transfer_Value * CarLine_Space) + Circle_Radius) * View_Rotate_X_Plus, (((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius) * View_Rotate_X_Minus, View_Rotate_X_Minus, View_Rotate_X_Plus
'        End If
        circle_horizontal.Name = "CATIA_CarLine_Circle"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_horizontal
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- Arrow 생성
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add(i - (X_Transfer_Value * CarLine_Space), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin, i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine(i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 2.5), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
       '--------------------------------------------------------------------------------------------------- Targ Position Type 선택
        If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
            Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
            Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 4) + Circle_Radius + Y_Minus_Margin
        ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
            Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
            Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius
        ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
            Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.5))
            Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.6))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius / 1.3)
        ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
            Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.4))
            Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.6))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius
        End If
        '--------------------------------------------------------------------------------------------------- Position_Text 생성(가로)
        If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
            If X1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf X1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Y1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Y1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Z1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Z1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            End If
        ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
            If X1 = 1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf X1 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 = 1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 = 1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf X1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                T_Value = 0
            ElseIf X1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                T_Value = 0
            ElseIf Y1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                L_Value = 0
            ElseIf Y1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                L_Value = 0
            ElseIf Z1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                H_Value = 0
            ElseIf Z1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                H_Value = 0
            End If
        ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
            If X1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf X1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            End If
        End If
        Position_Text.Name = "CATIA_CarLine_Text"
        '--------------------------------------------------------------------------------------------------- Position_Text Font Size 변경(가로)
        'Position_Text_Size = Circle_Radius / 2
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set Number_Text = Selection_View_Object.Texts.Add(i * Horizental_Marking_Value, i, (CEILING_YCenter(2) * CarLine_Space) + Circle_Radius + (Circle_Radius * 0.5) + Y_Minus_Margin)
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 1.7), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 4
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(가로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * Drawing_Scale
    Number_Text.AnchorPosition = 5
    Number_Text.X = i
    Number_Text.Y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius * 1.5)
Next i
End Sub
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Bottom &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Bottom()
CarLine_Rotate_Point_Count = 1
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- Circle 생성 (가로)
For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count, 1) - (Circle_Radius * View_Rotate_Y_Minus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count, 2) - (Circle_Radius * View_Rotate_Y_Plus)
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(i - (X_Transfer_Value * CarLine_Space), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius, Circle_Radius)
        End If
        circle_horizontal.Name = "CATIA_CarLine_Circle"
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (가로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine((i - (X_Transfer_Value * CarLine_Space) - Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius, (i - (X_Transfer_Value * CarLine_Space) + Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius)
        End If
        circle_horizontal_Line.Name = "CATIA_CarLine_Circle_Line"
        
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_horizontal
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- Arrow 생성
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add(i - (X_Transfer_Value * CarLine_Space), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin, i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine(i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 2.5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
    '--------------------------------------------------------------------------------------------------- Targ Position Type 선택
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 3))
        Position_Text_Horizontal_x2 = (i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 4) - Circle_Radius
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 3))
        Position_Text_Horizontal_x2 = (ii - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 3)
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.5))
        Position_Text_Horizontal_x2 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 4) - Circle_Radius
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.4))
        Position_Text_Horizontal_x2 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3
    End If
    '--------------------------------------------------------------------------------------------------- Position_Text 생성(가로)
    If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Y1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Y1X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Z1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Z1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X1 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        End If
    End If
        Position_Text.Name = "CATIA_CarLine_Text"
        '--------------------------------------------------------------------------------------------------- Position_Text Font Size 변경(가로)
        'Position_Text_Size = Circle_Radius / 2 * Drawing_Scale
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
        '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Horizental_Marking_Value, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) - Y_Max_Margin - Circle_Radius)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Horizental_Marking_Value, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 1.7), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) - Y_Max_Margin - Circle_Radius)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Horizental_Marking_Value, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) - Y_Max_Margin - Circle_Radius)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 4), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 1.7), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.1), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.3), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 4
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.2), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.4), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(가로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * DrawingView.Scale2
    Number_Text.Name = "CATIA_CarLine_Text"
    Number_Text.AnchorPosition = 5
    Number_Text.X = i
    Number_Text.Y = (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius * 1.5)
Next i
End Function
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Left &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Left()
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- Circle 생성 (세로)
For i = ((CEILING_YCenter(1) + 1) * CarLine_Space) To ((CEILING_YCenter(2) - X_Minus_Margin) * CarLine_Space) Step CarLine_Space
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count, 1) - (Circle_Radius * View_Rotate_Y_Plus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Minus)
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(CEILING_XCenter(1) * CarLine_Space + X_Plus_Margin - Circle_Radius, i, Circle_Radius)
        End If
        circle_vertical.Name = "CATIA_CarLine_Circle"
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (가로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine((CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2) + X_Plus_Margin, i, CEILING_XCenter(1) * CarLine_Space + X_Plus_Margin, i)
        End If
        circle_horizontal_Line.Name = "CATIA_CarLine_Circle_Line"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_vertical
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- Arrow 생성(세로)
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add((CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space, i - (Y_Transfer_Value) * CarLine_Space, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space + Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine((CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space + Circle_Radius, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.5), i - (Y_Transfer_Value) * CarLine_Space + Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
    '--------------------------------------------------------------------------------------------------- Targ Position Type 선택(세로)
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Vertical_x1 = CEILING_XCenter(1) * CarLine_Space - Circle_Radius - (Circle_Radius / 3) + X_Plus_Margin
        Position_Text_Vertical_x2 = CEILING_XCenter(1) * CarLine_Space - Circle_Radius - (Circle_Radius / 5) + X_Plus_Margin
        Position_Text_Vertical_y = i - (Circle_Radius / 4)
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 3)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 5)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius * 1.1)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius)
        Position_Text_Vertical_y = i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3)
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius * 1.1)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 1.9)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
    End If
    '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
    If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" Then
        If X2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
        ElseIf X2 = 0 Then
            If Not Y2 = 1 Or Not Y2 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Not Y2 = 0 Then
                If Z2 > 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                ElseIf Z2 < 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                End If
            Else
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
        Else
            If Y2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Y2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
       End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
    End If
        Position_Text.Name = "CATIA_CarLine_Text"
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
        '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set Number_Text = Selection_View_Object.Texts.Add(i * 1, CEILING_XCenter(1) * CarLine_Space - Circle_Radius + X_Plus_Margin, i + (Circle_Radius * 0.5))
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If ii - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 6), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 1.6), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.4), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.8), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 4
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If ii - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.3), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.8), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(세로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * Drawing_Scale
    Number_Text.AnchorPosition = 5
    If CarLine_Rotate_Number = 1 Then
        Number_Text.AnchorPosition = 5
        Number_Text.X = CarLine_Rotate_Center_Point_X
        Number_Text.Y = CarLine_Rotate_Center_Point_Y + (Circle_Radius * 0.5)
        CarLine_Rotate_Point_Count = CarLine_Rotate_Point_Count + 1
    End If
Next i
End Function
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Right &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Right()
CarLine_Rotate_Point_Count = 1
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- Circle 생성 (세로)
For i = (CEILING_YCenter(1) * CarLine_Space) To (CEILING_YCenter(2) * CarLine_Space) Step CarLine_Space
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_X_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Plus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_X_End_Point(CarLine_Rotate_Point_Count, 2) - (Circle_Radius * View_Rotate_Y_Minus)
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle((CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space, Circle_Radius)
        End If
        circle_vertical.Name = "CATIA_CarLine_Circle"
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (세로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_vertical_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_vertical_Line = Selection_View_Object_Factory2D.CreateClosedCircle((CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space, Circle_Radius)
        End If
        circle_vertical_Line.Name = "CATIA_CarLine_Circle_Line"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_vertical
        Drawingselection.Add circle_vertical_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
        '--------------------------------------------------------------------------------------------------- Arrow 생성(세로)
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add((CEILING_XCenter(2) * CarLine_Space), i, (CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i + Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine((CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i + Circle_Radius, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 2.5), i + Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
    '--------------------------------------------------------------------------------------------------- Targ Position Type 선택(세로)
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.6)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 4))
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.6)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.4)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.4)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
    End If
    '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
    If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
        ElseIf X2 = 0 Then
            If Not Y2 = 1 Or Not Y2 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Not Y2 = 0 Then
                If Z2 > 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                ElseIf Z2 < 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                End If
            Else
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
        Else
            If Y2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Y2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
       End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
    End If
        Position_Text.Name = "CATIA_CarLine_Text"
        '--------------------------------------------------------------------------------------------------- Position_Text Font_Size 변경(세로)
        'Position_Text_Size = Circle_Radius / 2 * Drawing_Scale
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        If i - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius - (Circle_Radius / 6), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius - (Circle_Radius / 1.6), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius - (Circle_Radius), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 2), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space, i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.2), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.1), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(세로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * Drawing_Scale
    If CarLine_Rotate_Number = 1 Then
        Number_Text.AnchorPosition = 5
        Number_Text.X = CarLine_Rotate_Center_Point_X
        Number_Text.Y = CarLine_Rotate_Center_Point_Y + (Circle_Radius * 0.5)
        CarLine_Rotate_Point_Count = CarLine_Rotate_Point_Count + 1
    End If
Next i
End Function
'---------------------------------------------------------------------------------------------------
'  REF_Center_Line_Create &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function REF_Center_Line_Create(Selection_View_Value)
    '--------------------------------------------------------------------------------------------------- Drawing Axis Direction Define
    Call Drawing_Axis_Direction_Define(Selection_View_Value)
    '--------------------------------------------------------------------------------------------------- Center Point X Value
    If Horizental_Axis_Value = "X" Then
        Center_Point_X = CarLine_Components(0)
    ElseIf Horizental_Axis_Value = "Y" Then
        Center_Point_X = CarLine_Components(1)
    ElseIf Horizental_Axis_Value = "Z" Then
        Center_Point_X = CarLine_Components(2)
    End If
    '--------------------------------------------------------------------------------------------------- Center Point Y Value
    If Vertical_Axis_Value = "X" Then
        Center_Point_Y = CarLine_Components(0)
    ElseIf Vertical_Axis_Value = "Y" Then
        Center_Point_Y = CarLine_Components(1)
    ElseIf Vertical_Axis_Value = "Z" Then
        Center_Point_Y = CarLine_Components(2)
    End If
    '--------------------------------------------------------------------------------------------------- 회전 Position 계산
    Call CarLine_Point_Rotate_Function(Center_Point_X * Horizental_Marking_Value, Center_Point_Y * Vertical_Marking_Value, 1)
    '--------------------------------------------------------------------------------------------------- X 최소값 , Y 최대값 계산
    If View_Rotate_X_Start_Point(1, 1) < View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01 - 1, 1) Then
        X_Minus_Position = View_Rotate_X_Start_Point(1, 1)
    Else
        X_Minus_Position = View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count01 - 1, 1)
    End If
    If View_Rotate_Y_Start_Point(1, 2) < View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02 - 1, 2) Then
        Y_Plus_Position = View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count02 - 1, 2)
    Else
        Y_Plus_Position = View_Rotate_Y_Start_Point(1, 2)
    End If
    '--------------------------------------------------------------------------------------------------- Car_Line X
    Set Car_Line_horizontal = Selection_View_Object_Factory2D.CreateLine(X_Minus_Position, CarLine_Rotate_Y_Value, CarLine_Rotate_X_Value, CarLine_Rotate_Y_Value)
    Car_Line_horizontal.Name = "CATIA_CarLine_Horizontal_0"
    '--------------------------------------------------------------------------------------------------- Car_Line Y
    Set Car_Line_Vertical = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_X_Value, CarLine_Rotate_Y_Value, CarLine_Rotate_X_Value, Y_Plus_Position)
    Car_Line_Vertical.Name = "CATIA_CarLine_Vertical_0"
    Set visProperties1 = Drawingselection.VisProperties
    Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- 색상변경
    Drawingselection.Add Car_Line_horizontal
    Drawingselection.Add Car_Line_Vertical
    visProperties1.SetRealLineType 1, 1
    visProperties1.SetRealWidth 2, 1
    visProperties1.SetRealColor 255, 0, 0, 1
    Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- circle_horizontal 생성 ( Reference CarLine 생성할때... )
    Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_X_Value, Y_Plus_Position + (Circle_Radius * 1.5), (Circle_Radius * 1.5))
    circle_horizontal.Name = "CATIA_CarLine_Circle_Horizontal"
    '--------------------------------------------------------------------------------------------------- circle_circle_vertical 생성 ( Reference CarLine 생성할때... )
    Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(X_Minus_Position - (Circle_Radius * 1.5), CarLine_Rotate_Y_Value, (Circle_Radius * 1.5))
    circle_vertical.Name = "CATIA_CarLine_Circle_vertical "
    '--------------------------------------------------------------------------------------------------- Car_Line_horizontal 생성
    Set Car_Line_horizontal = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_X_Value - (Circle_Radius * 1.5), Y_Plus_Position + (Circle_Radius * 1.5), CarLine_Rotate_X_Value + (Circle_Radius * 1.5), Y_Plus_Position + (Circle_Radius * 1.5))
    Car_Line_horizontal.Name = "CATIA_CarLine_Horizontal_0"
    '--------------------------------------------------------------------------------------------------- Car_Line_horizontal 생성
    Set Car_Line_Vertical = Selection_View_Object_Factory2D.CreateLine(X_Minus_Position - (2 * (Circle_Radius * 1.5)), CarLine_Rotate_Y_Value, X_Minus_Position, CarLine_Rotate_Y_Value)
    Car_Line_Vertical.Name = "CATIA_CarLine_Car_Line_vertical_0"
    '--------------------------------------------------------------------------------------------------- circle_vertical REF Text
    Set Position_Text = Selection_View_Object.Texts.Add("Ref " & Vertical_Axis_Value, CarLine_Rotate_X_Value + ((Circle_Radius * 1.5) * 0.1), Y_Plus_Position + (Circle_Radius * 1.5) - ((Circle_Radius * 1.5) * 0.5))
    Position_Text.AnchorPosition = 5
    Position_Text.SetFontSize 0, 0, Position_Text_Size / 1.7 '* Drawing_Scale
    Position_Text.Name = "CATIA_CarLine_Car_Line_vertical_0"
    
    Set Number_Text = Selection_View_Object.Texts.Add("0", CarLine_Rotate_X_Value, Y_Plus_Position + (Circle_Radius * 1.5) + ((Circle_Radius * 1.5) * 0.5))
    Number_Text.AnchorPosition = 5
    Number_Text.SetFontSize 0, 0, Position_Text_Size / 1.7 '* Drawing_Scale
    Number_Text.Name = "CATIA_CarLine_Car_Line_vertical_0"
    '--------------------------------------------------------------------------------------------------- circle_Horizontal REF Text
    Set Position_Text = Selection_View_Object.Texts.Add("Ref " & Horizental_Axis_Value, X_Minus_Position - (Circle_Radius * 1.5) + ((Circle_Radius * 1.5) * 0.1), CarLine_Rotate_Y_Value - ((Circle_Radius * 1.5) * 0.5))
    Position_Text.AnchorPosition = 5
    Position_Text.SetFontSize 0, 0, Position_Text_Size / 1.7 '* Drawing_Scale
    Position_Text.Name = "CATIA_CarLine_Car_Line_vertical_0"
    Set Number_Text = Selection_View_Object.Texts.Add("0", X_Minus_Position - (Circle_Radius * 1.5), CarLine_Rotate_Y_Value + ((Circle_Radius * 1.5) * 0.5))
    Number_Text.AnchorPosition = 5
    Number_Text.SetFontSize 0, 0, Position_Text_Size / 1.7 '* Drawing_Scale
    Number_Text.Name = "CATIA_CarLine_Car_Line_vertical_0"
    
    Set visProperties1 = Drawingselection.VisProperties
    Drawingselection.Clear
    Drawingselection.Add circle_horizontal
    Drawingselection.Add circle_vertical
    Drawingselection.Add Car_Line_horizontal
    Drawingselection.Add Car_Line_Vertical
    visProperties1.SetRealLineType 1, 1
    Drawingselection.Clear
End Function
'---------------------------------------------------------------------------------------------------
'  Activate_CarLine_Delete &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function Activate_View_CarLine_Delete()
Dim CATIA_CarLine_Element(2000)
'--------------------------------------------------------------------------------------------------- CarLine_Element Search
Drawingselection.Search "Name=CATIA_CarLine*,all"
CATIA_CarLine_Element_Count = Drawingselection.Count
On Error Resume Next
    '--------------------------------------------------------------------------------------------------- CarLine_Element 없을때...
    If Not CATIA_CarLine_Element_Count = 0 Then
        For i = 1 To Drawingselection.Count
            Set CATIA_CarLine_Element(i) = Drawingselection.Item(i).Value
        Next
        Drawingselection.Clear
        For i = 1 To CATIA_CarLine_Element_Count
            If CATIA_CarLine_Element(i).Parent.Parent.Name = Selection_View_Object.Name Then
               'if DrawingSheet.name = Selection_View_Object.parent.parent.name
                    Drawingselection.Add CATIA_CarLine_Element(i)
                'End If
            End If
        Next
        Drawingselection.Delete
    End If
On Error GoTo 0
End Function
'---------------------------------------------------------------------------------------------------
'  targ_Position_Top_Option &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Top_Option() As Variant
On Error Resume Next
    '======Access CATIA ===================
    Set CATIA = GetObject(, "CATIA.Application")
        If Err.Number <> 0 Then
            MsgBox "Error! Please Run CATIA and Load Model to Make Section!"
            Exit Function
        End If
    On Error GoTo 0
 Set Document = CATIA.ActiveDocument
 Set Selection = Document.Selection
 Set DrawingSheets = Document.sheets
 Set DrawingSheet = DrawingSheets.ActiveSheet
 Set DrawingViews = DrawingSheet.Views
 Set DrawingView = DrawingViews.ActiveView
 Set Factory2D = DrawingView.Factory2D

 '============================================================================================ Projection_Plane 정보
 DrawingView.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
 For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
 '============================================================================================ Circle 생성 (가로)
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set circle_horizontal = Factory2D.CreateClosedCircle(i, (CEILING_YCenter(1) * CarLine_Space) + Circle_Radius, Circle_Radius)
        circle_horizontal.Name = "CATIA_CarLine"
        '======================================================================================= 색상변경
        'Call Color_Change(circle_horizontal)
 '============================================================================================ Center_Line 생성 (가로)
        Set circle_horizontal_Line = Factory2D.CreateLine((i - Circle_Radius), (CEILING_YCenter(1) * CarLine_Space) + Circle_Radius, (i + Circle_Radius), (CEILING_YCenter(1) * CarLine_Space) + Circle_Radius)
        circle_horizontal_Line.Name = "CATIA_CarLine"
        '======================================================================================= 색상변경
        'Call Color_Change(circle_horizontal_Line)
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_horizontal
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
 '============================================================================================ Arrow 생성
     ElseIf type3_option.Value = True Or type4_option.Value = True Then
        Set arrow_top = DrawingView.Arrows.Add(i, (CEILING_YCenter(1) * CarLine_Space), i + Circle_Radius, (CEILING_YCenter(1) * CarLine_Space) + Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top)
        Set arrow_top_Line = Factory2D.CreateLine(i + Circle_Radius, (CEILING_YCenter(1) * CarLine_Space) + Circle_Radius, i + (Circle_Radius * 2.5), (CEILING_YCenter(1) * CarLine_Space) + Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top_Line)
     End If
 '============================================================================================ Targ Position Type 선택
        If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
           Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
           Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
           Position_Text_Horizontal_y = (CEILING_YCenter(1) * CarLine_Space) - (Circle_Radius / 4) + Circle_Radius
'        ElseIf type2_option.Value = True Then
'           Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
'           Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
'           Position_Text_Horizontal_y = (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius
'        ElseIf type3_option.Value = True Then
'           Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.5))
'           Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.6))
'           Position_Text_Horizontal_y = (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.3)
'        ElseIf type4_option.Value = True Then
'           Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.4))
'           Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.6))
'           Position_Text_Horizontal_y = (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius
        End If
 '============================================================================================ Position_Text 생성(가로)
        If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
           If X1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
           ElseIf X1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
           ElseIf Y1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
           ElseIf Y1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
           ElseIf Z1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
           ElseIf Z1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
           End If
        ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" Then
           If X1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf X1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Y1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Y1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Z1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Z1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           End If
        ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
           If X1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf X1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Y1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Y1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Z1 = 1 Then
              Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           ElseIf Z1 = -1 Then
              Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
           End If
        End If
        Position_Text.Name = "CATIA_CarLine"
'============================================================================================ Position_Text Font Size 변경(가로)
        'Position_Text_Size = Circle_Radius / 2 * Drawing_Scale
        Position_Text.SetFontSize 0, 0, Position_Text_Size * DrawingView.Scale2
        'Position_Text.SetFontSize 0, 0, Position_Text_Size
        '======================================================================================= 색상변경
        'Call Color_Change(Position_Text)
'============================================================================================ Number_Text 생성(가로) , Targ type 1
     'Horizental_Axis_Value, Horizental_Marking_Value, Vertical_Axis_Value, Vertical_Marking_Value
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Horizental_Marking_Value, i - (Circle_Radius / 4), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius)
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * Horizental_Marking_Value, i - (Circle_Radius / 1.7), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius)
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Horizental_Marking_Value, i - (Circle_Radius), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius)
        End If
        Number_Text.Name = "CATIA_CarLine"
'============================================================================================ Number_Text 생성(가로) , Targ type 2
'     ElseIf type2_option.Value = True Then
'        If i = 0 Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i - (Circle_Radius / 4), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius / 6)
'        ElseIf 0 < i Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i - (Circle_Radius / 1.7), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius / 6)
'        ElseIf i < 0 Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i - (Circle_Radius), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius / 6)
'        End If
''============================================================================================ Number_Text 생성(가로) , Targ type 3
'     ElseIf type3_option.Value = True Then
'        If i = 0 Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.6), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius)
'        ElseIf 0 < i Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.2), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius)
'        ElseIf i < 0 Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius / 1.2), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius)
'        End If
'
''============================================================================================ Number_Text 생성(가로) , Targ type 4
'     ElseIf type4_option.Value = True Then
'        If i = 0 Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.6), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius / 6)
'        ElseIf 0 < i Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.2), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius / 6)
'        ElseIf i < 0 Then
'           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius / 1.2), (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius / 6)
'        End If
     End If
'============================================================================================ Number_Text Font Size 변경(가로)
     'Number_Text_Size = Circle_Radius / 2
        Number_Text.SetFontSize 0, 0, Number_Text_Size * DrawingView.Scale2
        Number_Text.AnchorPosition = 5
        Number_Text.X = i
        Number_Text.Y = (CEILING_YCenter(1) * CarLine_Space) + (Circle_Radius * 1.35)
     '======================================================================================= 색상변경
     'Call Color_Change(Number_Text)
Next i
End Function
 '---------------------------------------------------------------------------------------------------
'  targ_Position_Bottom_Option &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
 Public Function targ_Position_Bottom_Option() As Variant
 
 On Error Resume Next
    '======Access CATIA ===================
    Set CATIA = GetObject(, "CATIA.Application")
        If Err.Number <> 0 Then
            MsgBox "Error! Please Run CATIA and Load Model to Make Section!"
            Exit Function
        End If
    On Error GoTo 0
 
 Set Document = CATIA.ActiveDocument
 Set Selection = Document.Selection
 Set DrawingSheets = Document.sheets
 Set DrawingSheet = DrawingSheets.ActiveSheet
 Set DrawingViews = DrawingSheet.Views
 Set DrawingView = DrawingViews.ActiveView
 Set Factory2D = DrawingView.Factory2D
 '============================================================================================ Projection_Plane 정보
 DrawingView.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
 '============================================================================================ Circle 생성 (가로)
 For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set circle_horizontal = Factory2D.CreateClosedCircle(i, ((CEILING_YCenter(2) * CarLine_Space) - Circle_Radius), Circle_Radius)
        circle_horizontal.Name = "CATIA_CarLine"
        '======================================================================================= 색상변경
        'Call Color_Change(circle_horizontal)
 '============================================================================================ Center_Line 생성 (가로)
        Set circle_horizontal_Line = Factory2D.CreateLine((i - Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) - Circle_Radius, (i + Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) - Circle_Radius)
        circle_horizontal_Line.Name = "CATIA_CarLine"
        '======================================================================================= 색상변경
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_horizontal
        Drawingselection.Add circle_horizontal
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
 '============================================================================================ Arrow 생성
     ElseIf type3_option.Value = True Or type4_option.Value = True Then
        Set arrow_top = DrawingView.Arrows.Add(i, (CEILING_YCenter(2) * CarLine_Space), i + Circle_Radius, (CEILING_YCenter(2) * CarLine_Space) - Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top)
        Set arrow_top_Line = Factory2D.CreateLine(i + Circle_Radius, (CEILING_YCenter(2) * CarLine_Space) - Circle_Radius, i + (Circle_Radius * 2.5), (CEILING_YCenter(2) * CarLine_Space) - Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top_Line)
     End If
 '============================================================================================ Targ Position Type 선택
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
        Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
        Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 4) - Circle_Radius
     ElseIf type2_option.Value = True Then
        Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
        Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
        Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 3)
     ElseIf type3_option.Value = True Then
        Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.5))
        Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.5))
        Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 4) - Circle_Radius
     ElseIf type4_option.Value = True Then
        Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.4))
        Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.5))
        Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3
     End If
 '============================================================================================ Position_Text 생성(가로)
     If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf X1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Y1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Y1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Z1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Z1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        End If
     ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        End If
     ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        End If
     End If
     Position_Text.Name = "CATIA_CarLine"
'============================================================================================ Position_Text Font Size 변경(가로)
        'Position_Text_Size = Circle_Radius / 2
        Position_Text.SetFontSize 0, 0, Position_Text_Size * DrawingView.Scale2
        '======================================================================================= 색상변경
        'Call Color_Change(Position_Text)
'============================================================================================ Number_Text 생성(가로) , Targ type 1
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Horizental_Marking_Value, i - (Circle_Radius / 4), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * Horizental_Marking_Value, i - (Circle_Radius / 1.7), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Horizental_Marking_Value, i - (Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3)
        End If
'============================================================================================ Number_Text 생성(가로) , Targ type 2
     ElseIf type2_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i - (Circle_Radius / 4), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius * 1.2))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i - (Circle_Radius / 1.7), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius * 1.2))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i - (Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius * 1.2))
        End If
'============================================================================================ Number_Text 생성(가로) , Targ type 3
     ElseIf type3_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.5), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.1), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius / 1.3), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius / 1.5) + Circle_Radius / 3)
        End If
'============================================================================================ Number_Text 생성(가로) , Targ type 4
     ElseIf type4_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.5), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius * 1.2))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius * 1.2), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius * 1.2))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, i + (Circle_Radius / 1.4), (CEILING_YCenter(2) * CarLine_Space) - (Circle_Radius * 1.2))
        End If
     End If
     Number_Text.Name = "CATIA_CarLine"
'============================================================================================ Number_Text Font Size 변경(가로)
    Number_Text.SetFontSize 0, 0, Number_Text_Size * DrawingView.Scale2
    Number_Text.AnchorPosition = 5
    Number_Text.X = i
    Number_Text.Y = ((CEILING_YCenter(2) * CarLine_Space) - Circle_Radius) + (Circle_Radius * 0.35)
     '======================================================================================= 색상변경
     'Call Color_Change(Number_Text)
Next i
End Function
 '---------------------------------------------------------------------------------------------------
'  targ_Position_Left_Option &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Left_Option() As Variant

On Error Resume Next
    '======Access CATIA ===================
    Set CATIA = GetObject(, "CATIA.Application")
        If Err.Number <> 0 Then
            MsgBox "Error! Please Run CATIA and Load Model to Make Section!"
            Exit Function
        End If
    On Error GoTo 0
 
 Set Document = CATIA.ActiveDocument
 Set Selection = Document.Selection
 Set DrawingSheets = Document.sheets
 Set DrawingSheet = DrawingSheets.ActiveSheet
 Set DrawingViews = DrawingSheet.Views
 Set DrawingView = DrawingViews.ActiveView
 Set Factory2D = DrawingView.Factory2D

 '============================================================================================ Projection_Plane 정보
 DrawingView.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
 '============================================================================================ Circle 생성 (세로)
 For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set circle_vertical = Factory2D.CreateClosedCircle((CEILING_XCenter(1) * CarLine_Space) - Circle_Radius, i, Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(circle_vertical)
        circle_vertical.Name = "CATIA_CarLine"
 '============================================================================================ Center_Line 생성 (세로)
        Set circle_vertical_Line = Factory2D.CreateLine((CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2), i, (CEILING_XCenter(1) * CarLine_Space), i)
        '======================================================================================= 색상변경
        'Call Color_Change(circle_vertical_Line)
        circle_vertical_Line.Name = "CATIA_CarLine"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_vertical
        Drawingselection.Add circle_vertical_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
 '============================================================================================ Arrow 생성(세로)
     ElseIf type3_option.Value = True Or type4_option.Value = True Then
        Set arrow_top = DrawingView.Arrows.Add((CEILING_XCenter(1) * CarLine_Space), i, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius, i + Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top)
        Set arrow_top_Line = Factory2D.CreateLine((CEILING_XCenter(1) * CarLine_Space) - Circle_Radius, i + Circle_Radius, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2.5), i + Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top_Line)
     End If
 '============================================================================================ Targ Position Type 선택(세로)
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 3)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 5)
        Position_Text_Vertical_y = (i - (Circle_Radius / 4))
     ElseIf type2_option.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 3)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 5)
        Position_Text_Vertical_y = (i + (Circle_Radius / 1.5))
     ElseIf type3_option.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius * 1.1)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius)
        Position_Text_Vertical_y = i + (Circle_Radius / 1.3)
     ElseIf type4_option.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius * 1.1)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 1.9)
        Position_Text_Vertical_y = (i + (Circle_Radius * 1.7))
     End If
 '============================================================================================ Position_Text 생성(세로)
     If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        End If
     ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
     ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
     End If
     Position_Text.Name = "CATIA_CarLine"
 '============================================================================================ Position_Text Font_Size 변경(세로)
        'Position_Text_Size = Circle_Radius / 2
        Position_Text.SetFontSize 0, 0, Position_Text_Size * DrawingView.Scale2
        '======================================================================================= 색상변경
        'Call Color_Change(Position_Text)
 '============================================================================================ Number_Text 생성(세로) , Targ Type 1
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 4), i + (Circle_Radius / 1.5))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 2), i + (Circle_Radius / 1.5))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius), i + (Circle_Radius / 1.5))
        End If
'============================================================================================ Number_Text 생성(세로) , Targ Type 2
     ElseIf type2_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 4), i - (Circle_Radius / 6))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius / 2), i - (Circle_Radius / 6))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius - (Circle_Radius), i - (Circle_Radius / 6))
        End If
'============================================================================================ Number_Text 생성(세로) , Targ Type 3
     ElseIf type3_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2), i + (Circle_Radius * 1.7))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2.4), i + (Circle_Radius * 1.7))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2.8), i + (Circle_Radius * 1.7))
        End If
'============================================================================================ Number_Text 생성(세로) , Targ Type 4
     ElseIf type4_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2), i + (Circle_Radius / 1.3))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2.3), i + (Circle_Radius / 1.3))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2.8), i + (Circle_Radius / 1.3))
        End If
     End If
     Number_Text.Name = "CATIA_CarLine"
'============================================================================================ Number_Text Font Size 변경(세로)
    Number_Text.SetFontSize 0, 0, Number_Text_Size * DrawingView.Scale2
    Number_Text.AnchorPosition = 5
    Number_Text.X = (CEILING_XCenter(1) * CarLine_Space) - Circle_Radius
    Number_Text.Y = i + (Circle_Radius * 0.35)
     '======================================================================================= 색상변경
    'Call Color_Change(Number_Text)
 Next i
 End Function
  '---------------------------------------------------------------------------------------------------
'  targ_Position_Right_Option &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
 Public Function targ_Position_Right_Option() As Variant

 On Error Resume Next
    '======Access CATIA ===================
    Set CATIA = GetObject(, "CATIA.Application")
        If Err.Number <> 0 Then
            MsgBox "Error! Please Run CATIA and Load Model to Make Section!"
            Exit Function
        End If
    On Error GoTo 0
 
 Set Document = CATIA.ActiveDocument
 Set Selection = Document.Selection
 Set DrawingSheets = Document.sheets
 Set DrawingSheet = DrawingSheets.ActiveSheet
 Set DrawingViews = DrawingSheet.Views
 Set DrawingView = DrawingViews.ActiveView
 Set Factory2D = DrawingView.Factory2D

 '============================================================================================ Projection_Plane 정보
 DrawingView.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
 '============================================================================================ Circle 생성 (세로)
 For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set circle_vertical = Factory2D.CreateClosedCircle((CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i, Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(circle_vertical)
        circle_vertical.Name = "CATIA_CarLine"
 '============================================================================================ Center_Line 생성 (세로)
        Set circle_vertical_Line = Factory2D.CreateLine((CEILING_XCenter(2) * CarLine_Space) - (Circle_Radius * 2), i, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 2), i)
        '======================================================================================= 색상변경
        'Call Color_Change(circle_vertical_Line)
        circle_vertical_Line.Name = "CATIA_CarLine"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_vertical
        Drawingselection.Add circle_vertical_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
 '============================================================================================ Arrow 생성(세로)
     ElseIf type3_option.Value = True Or type4_option.Value = True Then
        Set arrow_top = DrawingView.Arrows.Add((CEILING_XCenter(2) * CarLine_Space), i, (CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i + Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top)
        Set arrow_top_Line = Factory2D.CreateLine((CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i + Circle_Radius, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 2.5), i + Circle_Radius)
        '======================================================================================= 색상변경
        'Call Color_Change(arrow_top_Line)
     End If
 '============================================================================================ Targ Position Type 선택(세로)
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.6)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.3)
        Position_Text_Vertical_y = (i - (Circle_Radius / 4))
     ElseIf type2_option.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.6)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.3)
        Position_Text_Vertical_y = (i + (Circle_Radius / 1.5))
     ElseIf type3_option.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.4)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.5)
        Position_Text_Vertical_y = (i + (Circle_Radius / 1.3))
     ElseIf type4_option.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.4)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.5)
        Position_Text_Vertical_y = (i + (Circle_Radius * 1.7))
     End If
 '============================================================================================ Position_Text 생성(세로)
     If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        End If
     ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
     ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
           Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
           Set Position_Text = DrawingView.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
     End If
     Position_Text.Name = "CATIA_CarLine"
 '============================================================================================ Position_Text Font_Size 변경(세로)
        'Position_Text_Size = Circle_Radius / 2
        Position_Text.SetFontSize 0, 0, Position_Text_Size * DrawingView.Scale2
        '======================================================================================= 색상변경
        'Call Color_Change(Position_Text)
 '============================================================================================ Number_Text 생성(세로) , Targ Type 1
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.3), i + (Circle_Radius / 1.5))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 2), i + (Circle_Radius / 1.5))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * Vertical_Marking_Value, (CEILING_XCenter(2) * CarLine_Space), i + (Circle_Radius / 1.5))
        End If
'============================================================================================ Number_Text 생성(세로) , Targ Type 2
     ElseIf type2_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.3), i - (Circle_Radius / 6))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 2), i - (Circle_Radius / 6))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space), i - (Circle_Radius / 6))
        End If
'============================================================================================ Number_Text 생성(세로) , Targ Type 3
     ElseIf type3_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.5), i + (Circle_Radius * 1.7))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.2), i + (Circle_Radius * 1.7))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.3), i + (Circle_Radius * 1.7))
        End If
'============================================================================================ Number_Text 생성(세로) , Targ Type 2
     ElseIf type4_option.Value = True Then
        If i = 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.5), i + (Circle_Radius / 1.3))
        ElseIf 0 < i Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 1.1), i + (Circle_Radius / 1.3))
        ElseIf i < 0 Then
           Set Number_Text = DrawingView.Texts.Add(i * 1, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius / 1.3), i + (Circle_Radius / 1.3))
        End If
     End If
     Number_Text.Name = "CATIA_CarLine"
'============================================================================================ Number_Text Font Size 변경(세로)
    Number_Text.SetFontSize 0, 0, Number_Text_Size * DrawingView.Scale2
    Number_Text.AnchorPosition = 5
    Number_Text.X = (CEILING_XCenter(2) * CarLine_Space) + Circle_Radius
    Number_Text.Y = i + (Circle_Radius * 0.35)
    '======================================================================================= 색상변경
     'Call Color_Change(Number_Text)
 Next i
End Function
'---------------------------------------------------------------------------------------------------
'  targ_Position_Top_Ref &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub targ_Position_Top_Ref()
CarLine_Rotate_Point_Count = 1
'For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
'For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
    '--------------------------------------------------------------------------------------------------- Circle 생성 (가로)
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_Y_Start_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus)
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(i, CEILING_YCenter(2) * CarLine_Space + Circle_Radius + Y_Minus_Margin, Circle_Radius)
        End If
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (가로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine((i - Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) + Circle_Radius + Y_Minus_Margin, (i + Circle_Radius), (CEILING_YCenter(2) * CarLine_Space) + Circle_Radius + Y_Minus_Margin)
        End If
        circle_horizontal_Line.Name = "CATIA_CarLine_Circle_Line"
'        If CarLine_Rotate_Number = 1 Then
'            circle_horizontal_Line.SetData (i - (X_Transfer_Value * CarLine_Space) + Circle_Radius) * View_Rotate_X_Plus, (((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius) * View_Rotate_X_Minus, View_Rotate_X_Minus, View_Rotate_X_Plus
'        End If
        circle_horizontal.Name = "CATIA_CarLine_Circle"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_horizontal
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- Arrow 생성
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add(i - (X_Transfer_Value * CarLine_Space), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin, i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine(i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 2.5), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + Y_Max_Margin + Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
       '--------------------------------------------------------------------------------------------------- Targ Position Type 선택
        If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
            Position_Text_Horizontal_x1 = (CarLine_Rotate_Center_Point_X - (Circle_Radius / 3))
            Position_Text_Horizontal_x2 = (CarLine_Rotate_Center_Point_X - (Circle_Radius / 5))
            Position_Text_Horizontal_y = CarLine_Rotate_Center_Point_Y - (Circle_Radius / 4) + Circle_Radius + Y_Minus_Margin
        ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
            Position_Text_Horizontal_x1 = (i - (Circle_Radius / 3))
            Position_Text_Horizontal_x2 = (i - (Circle_Radius / 5))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius
        ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
            Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.5))
            Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.6))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius / 1.3)
        ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
            Position_Text_Horizontal_x1 = (i + (Circle_Radius * 1.4))
            Position_Text_Horizontal_x2 = (i + (Circle_Radius * 1.6))
            Position_Text_Horizontal_y = (CEILING_YCenter(2) * CarLine_Space) + (Circle_Radius / 1.5) + Circle_Radius
        End If
        '--------------------------------------------------------------------------------------------------- Position_Text 생성(가로)
        If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
            If X1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf X1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Y1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Y1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Z1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            ElseIf Z1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
            End If
        ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
            If X1 = 1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf X1 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 = 1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 = 1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf X1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                T_Value = 0
            ElseIf X1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                T_Value = 0
            ElseIf Y1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                L_Value = 0
            ElseIf Y1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                L_Value = 0
            ElseIf Z1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                H_Value = 0
            ElseIf Z1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
                H_Value = 0
            End If
        ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
            If X1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf X1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Y1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            ElseIf Z1 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
            End If
        End If
        Position_Text.Name = "CATIA_CarLine_Text"
        '--------------------------------------------------------------------------------------------------- Position_Text Font Size 변경(가로)
        'Position_Text_Size = Circle_Radius / 2
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set Number_Text = Selection_View_Object.Texts.Add(i * Horizental_Marking_Value + CarLine_Margin_Value_X, i, (CEILING_YCenter(2) * CarLine_Space) + Circle_Radius + (Circle_Radius * 0.5) + Y_Minus_Margin)
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 1.7), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 4
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If i - (X_Transfer_Value * CarLine_Space) = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf 0 < i - (X_Transfer_Value * CarLine_Space) Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        ElseIf i - (X_Transfer_Value * CarLine_Space) < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.2), ((CEILING_YCenter(2) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) + Y_Max_Margin + Circle_Radius / 6)
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(가로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * DrawingView.Scale2
    Number_Text.AnchorPosition = 5
    If CarLine_Rotate_Number = 1 Then
        Number_Text.AnchorPosition = 5
        Number_Text.X = CarLine_Rotate_Center_Point_X
        Number_Text.Y = CarLine_Rotate_Center_Point_Y + (Circle_Radius * 0.5)
        CarLine_Rotate_Point_Count = CarLine_Rotate_Point_Count + 1
    End If
Next i
End Sub
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Bottom &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Bottom_Ref()
CarLine_Rotate_Point_Count = 1
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- Circle 생성 (가로)
For i = ((CEILING_XCenter(1) + 1) * CarLine_Space) To ((CEILING_XCenter(2) - 1) * CarLine_Space) Step CarLine_Space
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) - (Circle_Radius * View_Rotate_Y_Minus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) - (Circle_Radius * View_Rotate_Y_Plus)
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_horizontal = Selection_View_Object_Factory2D.CreateClosedCircle(i - (X_Transfer_Value * CarLine_Space), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius, Circle_Radius)
        End If
        circle_horizontal.Name = "CATIA_CarLine_Circle"
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (가로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine((i - (X_Transfer_Value * CarLine_Space) - Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius, (i - (X_Transfer_Value * CarLine_Space) + Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius)
        End If
        circle_horizontal_Line.Name = "CATIA_CarLine_Circle_Line"
        
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_horizontal
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- Arrow 생성
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add(i - (X_Transfer_Value * CarLine_Space), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin, i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine(i - (X_Transfer_Value * CarLine_Space) + Circle_Radius, ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 2.5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
    '--------------------------------------------------------------------------------------------------- Targ Position Type 선택
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 3))
        Position_Text_Horizontal_x2 = (i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 4) - Circle_Radius
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 3))
        Position_Text_Horizontal_x2 = (ii - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 5))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 3)
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.5))
        Position_Text_Horizontal_x2 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 4) - Circle_Radius
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Position_Text_Horizontal_x1 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.4))
        Position_Text_Horizontal_x2 = (i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.6))
        Position_Text_Horizontal_y = ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3
    End If
    '--------------------------------------------------------------------------------------------------- Position_Text 생성(가로)
    If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Y1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Y1X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Z1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        ElseIf Z1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Horizontal_x1, Position_Text_Horizontal_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X1 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf X1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Y1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        ElseIf Z1 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Horizontal_x2, Position_Text_Horizontal_y)
        End If
    End If
        Position_Text.Name = "CATIA_CarLine_Text"
        '--------------------------------------------------------------------------------------------------- Position_Text Font Size 변경(가로)
        'Position_Text_Size = Circle_Radius / 2 * Drawing_Scale
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set Number_Text = Selection_View_Object.Texts.Add(i * Horizental_Marking_Value + CarLine_Margin_Value_X, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 4), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) + (Circle_Radius / 1.5) - Y_Max_Margin - Circle_Radius)
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 4), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius / 1.7), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) - (Circle_Radius), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.1), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3)
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.3), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius / 1.5) + Circle_Radius / 3)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(가로) , Targ type 4
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.5), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius * 1.2), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (X_Transfer_Value * CarLine_Space) * 1, i - (X_Transfer_Value * CarLine_Space) + (Circle_Radius / 1.4), ((CEILING_YCenter(1) - Y_Transfer_Value) * CarLine_Space) - Y_Max_Margin - (Circle_Radius * 1.2))
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(가로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * DrawingView.Scale2
    Number_Text.Name = "CATIA_CarLine_Text"
    If CarLine_Rotate_Number = 1 Then
        Number_Text.AnchorPosition = 5
        Number_Text.X = CarLine_Rotate_Center_Point_X
        Number_Text.Y = CarLine_Rotate_Center_Point_Y + (Circle_Radius * 0.5)
        CarLine_Rotate_Point_Count = CarLine_Rotate_Point_Count + 1
    End If
Next i
End Function
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Left &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Left_Ref()
CarLine_Rotate_Point_Count = 1
X_Minus_Margin = 0
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- Circle 생성 (세로)
For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count, 1) - (Circle_Radius * View_Rotate_Y_Plus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_X_Start_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Minus)
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(CEILING_XCenter(1) * CarLine_Space + X_Plus_Margin - Circle_Radius, i, Circle_Radius)
        End If
        circle_vertical.Name = "CATIA_CarLine_Circle"
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (가로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_horizontal_Line = Selection_View_Object_Factory2D.CreateLine((CEILING_XCenter(1) * CarLine_Space) - (Circle_Radius * 2) + X_Plus_Margin, i, CEILING_XCenter(1) * CarLine_Space + X_Plus_Margin, i)
        End If
        circle_horizontal_Line.Name = "CATIA_CarLine_Circle_Line"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_vertical
        Drawingselection.Add circle_horizontal_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
    '--------------------------------------------------------------------------------------------------- Arrow 생성(세로)
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add((CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space, i - (Y_Transfer_Value) * CarLine_Space, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space + Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine((CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space + Circle_Radius, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.5), i - (Y_Transfer_Value) * CarLine_Space + Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
    '--------------------------------------------------------------------------------------------------- Targ Position Type 선택(세로)
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Vertical_x1 = CEILING_XCenter(1) * CarLine_Space - Circle_Radius - (Circle_Radius / 3) + X_Plus_Margin
        Position_Text_Vertical_x2 = CEILING_XCenter(1) * CarLine_Space - Circle_Radius - (Circle_Radius / 5) + X_Plus_Margin
        Position_Text_Vertical_y = i - (Circle_Radius / 4)
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 3)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 5)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius * 1.1)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius)
        Position_Text_Vertical_y = i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3)
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius * 1.1)
        Position_Text_Vertical_x2 = (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 1.9)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
    End If
    '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
    If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" Then
        If X2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
        ElseIf X2 = 0 Then
            If Not Y2 = 1 Or Not Y2 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Not Y2 = 0 Then
                If Z2 > 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                ElseIf Z2 < 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                End If
            Else
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
        Else
            If Y2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Y2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
       End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
    End If
        Position_Text.Name = "CATIA_CarLine_Text"
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
        '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 1
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value + CarLine_Margin_Value_Y, CEILING_XCenter(1) * CarLine_Space - Circle_Radius + X_Plus_Margin, i + (Circle_Radius * 0.5))
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If ii - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 6), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius / 1.6), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - Circle_Radius - (Circle_Radius), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.4), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.8), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 4
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If ii - (Y_Transfer_Value) * CarLine_Space = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf 0 < i - (Y_Transfer_Value) * CarLine_Space Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.3), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf i - (Y_Transfer_Value) * CarLine_Space < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(1) - X_Transfer_Value) * CarLine_Space - (Circle_Radius * 2.8), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(세로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * DrawingView.Scale2
    Number_Text.AnchorPosition = 5
    If CarLine_Rotate_Number = 1 Then
        Number_Text.AnchorPosition = 5
        Number_Text.X = CarLine_Rotate_Center_Point_X
        Number_Text.Y = CarLine_Rotate_Center_Point_Y + (Circle_Radius * 0.5)
        CarLine_Rotate_Point_Count = CarLine_Rotate_Point_Count + 1
    End If
Next i
End Function
'---------------------------------------------------------------------------------------------------
'  Targ_Position_Right &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function targ_Position_Right_Ref()
CarLine_Rotate_Point_Count = 1
'--------------------------------------------------------------------------------------------------- Projection_Plane 정보
Selection_View_Object.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- Circle 생성 (세로)
For i = ((CEILING_YCenter(1) - 1) * CarLine_Space) To ((CEILING_YCenter(2) + 1) * CarLine_Space) Step -CarLine_Space
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Or CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If CarLine_Rotate_Number = 1 Then
            CarLine_Rotate_Center_Point_X = View_Rotate_X_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Plus)
            CarLine_Rotate_Center_Point_Y = View_Rotate_X_End_Point(CarLine_Rotate_Point_Count, 2) - (Circle_Radius * View_Rotate_Y_Minus)
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle(CarLine_Rotate_Center_Point_X, CarLine_Rotate_Center_Point_Y, Circle_Radius)
        Else
            Set circle_vertical = Selection_View_Object_Factory2D.CreateClosedCircle((CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space, Circle_Radius)
        End If
        circle_vertical.Name = "CATIA_CarLine_Circle"
        '--------------------------------------------------------------------------------------------------- Center_Line 생성 (세로)
        If CarLine_Rotate_Number = 1 Then
            Set circle_vertical_Line = Selection_View_Object_Factory2D.CreateLine(CarLine_Rotate_Center_Point_X - (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y + (Circle_Radius * View_Rotate_Y_Minus), CarLine_Rotate_Center_Point_X + (Circle_Radius * View_Rotate_Y_Plus), CarLine_Rotate_Center_Point_Y - (Circle_Radius * View_Rotate_Y_Minus))
        Else
            Set circle_vertical_Line = Selection_View_Object_Factory2D.CreateClosedCircle((CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius, i - (Y_Transfer_Value) * CarLine_Space, Circle_Radius)
        End If
        circle_vertical_Line.Name = "CATIA_CarLine_Circle_Line"
        Set visProperties1 = Drawingselection.VisProperties
        Drawingselection.Clear
        Drawingselection.Add circle_vertical
        Drawingselection.Add circle_vertical_Line
        visProperties1.SetRealWidth 1, 1
        visProperties1.SetRealLineType 1, 1
        Drawingselection.Clear
        '--------------------------------------------------------------------------------------------------- Arrow 생성(세로)
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Or CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Set arrow_top = Selection_View_Object.Arrows.Add((CEILING_XCenter(2) * CarLine_Space), i, (CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i + Circle_Radius)
        arrow_top.Name = "CATIA_CarLine_Arrow"
        Set arrow_top_Line = Selection_View_Object_Factory2D.CreateLine((CEILING_XCenter(2) * CarLine_Space) + Circle_Radius, i + Circle_Radius, (CEILING_XCenter(2) * CarLine_Space) + (Circle_Radius * 2.5), i + Circle_Radius)
        arrow_top_Line.Name = "CATIA_CarLine_Arrow_Line"
    End If
    '--------------------------------------------------------------------------------------------------- Targ Position Type 선택(세로)
    If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.6)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 4))
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.6)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.4)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        Position_Text_Vertical_x1 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.4)
        Position_Text_Vertical_x2 = (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5)
        Position_Text_Vertical_y = (i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
    End If
    '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
    If CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("BL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("WL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("TL", Position_Text_Vertical_x1, Position_Text_Vertical_y)
        End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" = True Then
        If X2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("T", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = 1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 = -1 Then
            Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        '--------------------------------------------------------------------------------------------------- Position_Text 생성(세로)
        ElseIf X2 = 0 Then
            If Not Y2 = 1 Or Not Y2 = -1 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Not Y2 = 0 Then
                If Z2 > 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                ElseIf Z2 < 0 Then
                    Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
                End If
            Else
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
        Else
            If Y2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Y2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("L", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 > 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            ElseIf Z2 < 0 Then
                Set Position_Text = Selection_View_Object.Texts.Add("H", Position_Text_Vertical_x2, Position_Text_Vertical_y)
            End If
       End If
    ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
        If X2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf X2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("X", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Y2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Y", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 > 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        ElseIf Z2 < 0 Then
            Set Position_Text = Selection_View_Object.Texts.Add("Z", Position_Text_Vertical_x2, Position_Text_Vertical_y)
        End If
    End If
        Position_Text.Name = "CATIA_CarLine_Text"
        '--------------------------------------------------------------------------------------------------- Position_Text Font_Size 변경(세로)
        'Position_Text_Size = Circle_Radius / 2 * Drawing_Scale
        Position_Text.SetFontSize 0, 0, Position_Text_Size / 2 * DrawingView.Scale2
        If CarLine_Rotate_Number = 1 Then
            Position_Text.AnchorPosition = 5
            Position_Text.X = CarLine_Rotate_Center_Point_X + (Circle_Radius * 0.1)
            Position_Text.Y = CarLine_Rotate_Center_Point_Y - (Circle_Radius * 0.5)
            'Number_Text.SetData View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 1) + (Circle_Radius * View_Rotate_Y_Minus) + (Circle_Radius / 1.5), View_Rotate_Y_End_Point(CarLine_Rotate_Point_Count, 2) + (Circle_Radius * View_Rotate_Y_Plus), Circle_Radius - (Circle_Radius / 1.5)
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 1
     If CAR_Line_Form.Car_Line_Type_Option1.Value = True Then
        Set Number_Text = Selection_View_Object.Texts.Add(i * Vertical_Marking_Value + CarLine_Margin_Value_Y, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + Circle_Radius - (Circle_Radius / 6), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.5))
        '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option2.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 2), i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space, i - (Y_Transfer_Value) * CarLine_Space - (Circle_Radius / 6))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 3
    ElseIf CAR_Line_Form.Car_Line_Type_Option3.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.2), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.7))
        End If
    '--------------------------------------------------------------------------------------------------- Number_Text 생성(세로) , Targ Type 2
    ElseIf CAR_Line_Form.Car_Line_Type_Option4.Value = True Then
        If i = 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.5), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf 0 < i Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius * 1.1), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        ElseIf i < 0 Then
            Set Number_Text = Selection_View_Object.Texts.Add(i - (Y_Transfer_Value) * CarLine_Space * 1, (CEILING_XCenter(2) - X_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3), i - (Y_Transfer_Value) * CarLine_Space + (Circle_Radius / 1.3))
        End If
    End If
    Number_Text.Name = "CATIA_CarLine_Text"
    '--------------------------------------------------------------------------------------------------- Number_Text Font Size 변경(세로)
    'Number_Text_Size = Circle_Radius / 2 * Drawing_Scale
    Number_Text.SetFontSize 0, 0, Number_Text_Size / 2 * DrawingView.Scale2
    If CarLine_Rotate_Number = 1 Then
        Number_Text.AnchorPosition = 5
        Number_Text.X = CarLine_Rotate_Center_Point_X
        Number_Text.Y = CarLine_Rotate_Center_Point_Y + (Circle_Radius * 0.5)
        CarLine_Rotate_Point_Count = CarLine_Rotate_Point_Count + 1
    End If
Next i
End Function
'---------------------------------------------------------------------------------------------------
'  CarLine_Point_Rotate_Function &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function CarLine_Point_Rotate_Function(X_Position_Value, Y_Position_Value, Ref_Position_Value)
Dim Radian_Angle As Double, PI As Double
'--------------------------------------------------------------------------------------------------- PI , Radian_Angle 정의
PI = 3.14159265358979
Radian_Angle = 180 / PI
'--------------------------------------------------------------------------------------------------- 원점으로 거리
Line_Length_Value = Sqr((X_Position_Value * X_Position_Value) + (Y_Position_Value * Y_Position_Value))
'--------------------------------------------------------------------------------------------------- 회전각도
Rotate_Angle = Round((Atn(-View_Rotate_X_Minus / Sqr(-View_Rotate_X_Minus * View_Rotate_X_Minus + 1)) + 2 * Atn(1)) * Radian_Angle, 2)
'--------------------------------------------------------------------------------------------------- - 회전 각도 적용
If View_Rotate_X_Minus * View_Rotate_X_Plus < 0 Then
    Rotate_Angle = Rotate_Angle - 180
End If
If Ref_Position_Value = 0 Then
    Rotate_Angle = Rotate_Angle * -1
End If
'--------------------------------------------------------------------------------------------------- 계산각도
If X_Position_Value = 0 Then
    X_Position_Value = 0.001
    Calculation_Angle = (Atn(Y_Position_Value / X_Position_Value) * Radian_Angle)
Else
    Calculation_Angle = (Atn(Y_Position_Value / X_Position_Value) * Radian_Angle)
End If
'--------------------------------------------------------------------------------------------------- 초기 각도
If X_Position_Value >= 0 And Y_Position_Value >= 0 Then
    Inicial_Angle = Calculation_Angle
ElseIf X_Position_Value < 0 And Y_Position_Value >= 0 Then
    Inicial_Angle = Calculation_Angle + 180
ElseIf X_Position_Value < 0 And Y_Position_Value < 0 Then
    Inicial_Angle = Calculation_Angle + 180
Else
    Inicial_Angle = Calculation_Angle + 360
End If
CarLine_Rotate_X_Value = Line_Length_Value * Cos((Rotate_Angle + Inicial_Angle) / Radian_Angle)
CarLine_Rotate_Y_Value = Line_Length_Value * Sin((Rotate_Angle + Inicial_Angle) / Radian_Angle)
End Function
'---------------------------------------------------------------------------------------------------
' Drawing_Axis_Direction_Define &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function Drawing_Axis_Direction_Define(Drawing_Value)
Drawing_Value.GenerativeBehavior.GetProjectionPlane X1, Y1, Z1, X2, Y2, Z2
'--------------------------------------------------------------------------------------------------- X축 0일때...
If X1 = 0 And X2 = 0 Then
    If Y1 < 0 And Z2 > 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = 1
    ElseIf Y1 > 0 And Z2 > 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = 1
    ElseIf Z1 > 0 And Y2 > 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = 1
    ElseIf Z1 > 0 And Y2 < 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = -1
    ElseIf Y1 > 0 And Z2 < 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = -1
    ElseIf Y1 < 0 And Z2 < 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = -1
    ElseIf Z1 < 0 And Y2 < 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = -1
    ElseIf Z1 < 0 And Y2 > 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = 1
    End If
'--------------------------------------------------------------------------------------------------- Y축 0일때...
ElseIf Y1 = 0 And Y2 = 0 Then
    If Z1 < 0 And X2 > 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = 1
    ElseIf Z1 > 0 And X2 > 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = 1
    ElseIf X1 > 0 And Z2 > 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = 1
    ElseIf X1 > 0 And Z2 < 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = -1
    ElseIf Z1 > 0 And X2 < 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = -1
    ElseIf Z1 < 0 And X2 < 0 Then
        Horizental_Axis_Value = "Z"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = -1
    ElseIf X1 < 0 And Z2 < 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = -1
    ElseIf X1 < 0 And Z2 > 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Z"
        Vertical_Marking_Value = 1
    End If
'--------------------------------------------------------------------------------------------------- Z축 0일때...
ElseIf Z1 = 0 And Z2 = 0 Then
    If Y1 > 0 And X2 > 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = 1
    ElseIf Y1 < 0 And X2 > 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = 1
    ElseIf X1 > 0 And Y2 < 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = -1
    ElseIf X1 > 0 And Y2 > 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = 1
    ElseIf Y1 < 0 And X2 < 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = -1
    ElseIf Y1 > 0 And X2 < 0 Then
        Horizental_Axis_Value = "Y"
        Horizental_Marking_Value = 1
        Vertical_Axis_Value = "X"
        Vertical_Marking_Value = -1
    ElseIf X1 < 0 And Y2 > 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = 1
    ElseIf X1 < 0 And Y2 < 0 Then
        Horizental_Axis_Value = "X"
        Horizental_Marking_Value = -1
        Vertical_Axis_Value = "Y"
        Vertical_Marking_Value = -1
    End If
End If
End Function
'---------------------------------------------------------------------------------------------------
' Drawing_View_Selection &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub Drawing_View_Selection(View_Label)
Call CATIA_Basic_Drawing
'--------------------------------------------------------------------------------------------------- selection.Count = "0"
If Drawingselection.Count = "0" Then
      Select_error = MsgBox("선택된 Drawing View가 없습니다", vbInformation, "CATIA Automation")
      Exit Sub
End If
'--------------------------------------------------------------------------------------------------- Poly Line Selection
Set Selection_View_Object = Drawingselection.Item(1).Value
View_Label.Caption = Selection_View_Object.Name
Drawingselection.Clear
'--------------------------------------------------------------------------------------------------- View Size 가져오기
Selection_View_Object.Activate
Selection_View_Object.Size View_Size
Xmin = View_Size(0)
Xmax = View_Size(1)
Ymin = View_Size(2)
Ymax = View_Size(3)
'If View_Label = CAR_Line_Form.Move_View_Selection_Label Then
    CAR_Line_Form.Selection_View_X_Value_Text.Text = Round(Selection_View_Object.X, 2)
    CAR_Line_Form.Selection_View_Y_Value_Text.Text = Round(Selection_View_Object.Y, 2)
'End If
End Sub
'---------------------------------------------------------------------------------------------------
' CJRound &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function CJRound(c_val As Double, c_opt As Integer, c_unit As Integer) As Double
Dim i As Integer
Dim t_s As String, t_u As Double
t_u = 10 ^ (c_unit * -1)
Select Case c_opt
    Case 2: t_u = Int((c_val + (t_u / 2)) / t_u) * t_u               ' 사사오입
    Case 3: t_u = Int((c_val + (t_u - 10 ^ (-1 * (15 - Len(Format(Int(c_val))))))) / t_u) * t_u   ' 절상
    '       Case 3: t_u = Int((c_val + (t_u - 10 ^ -15)) / t_u) * t_u   ' <---- 요렇게하면 CJRound(1234567,3,0) --> 1234568 Return됨
    Case 4: t_u = Int(c_val / t_u) * t_u                             ' 절사
    Case Else: t_u = c_val
End Select
If c_unit > 0 Then
    t_s = "0."
    For i = 1 To c_unit
        t_s = t_s + "0"
    Next i
    CJRound = Val(Format(t_u, t_s))
Else
    CJRound = t_u
End If
End Function
'---------------------------------------------------------------------------------------------------
'  CarLine_Combo_Image_Setting &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub CarLine_Combo_Image_Setting()
'--------------------------------------------------------------------------------------------------- Image 초기화
For i = 0 To CAR_Line_Form.File_numbering.ListCount - 1
    Image_Name = Split(CAR_Line_Form.File_numbering.List(i), ".jpg")
    CAR_Line_Form.Controls.Item(CStr(Image_Name(0))).Visible = False
Next
'--------------------------------------------------------------------------------------------------- Combo 값 가져오기
If CAR_Line_Form.Car_Line_Text_Size_Text.Text = "2.5" Then
    Car_Line_Circle_Text_Image = "25"
ElseIf CAR_Line_Form.Car_Line_Text_Size_Text.Text = "5" Then
    Car_Line_Circle_Text_Image = "50"
ElseIf CAR_Line_Form.Car_Line_Text_Size_Text.Text = "7.5" Then
    Car_Line_Circle_Text_Image = "75"
ElseIf CAR_Line_Form.Car_Line_Text_Size_Text.Text = "10" Then
    Car_Line_Circle_Text_Image = "100"
End If
If CAR_Line_Form.Car_Line_Position_Combo.Text = "T,L,H" Then
    Car_Line_Position_Image = "TLH"
ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "X,Y,Z" Then
    Car_Line_Position_Image = "XYZ"
ElseIf CAR_Line_Form.Car_Line_Position_Combo.Text = "TL,BL,WL" Then
    Car_Line_Position_Image = "TLBLWL"
End If
'--------------------------------------------------------------------------------------------------- Image Show
CAR_Line_Form.Controls.Item(CStr(Car_Line_Position_Image & "_" & Car_Line_Circle_Text_Image & "_" & CAR_Line_Form.Car_Line_Circle_Size_Text.Text)).Visible = True

End Sub






















