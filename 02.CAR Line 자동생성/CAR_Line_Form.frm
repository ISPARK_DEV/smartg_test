VERSION 5.00
Begin VB.Form CAR_Line_Form 
   BackColor       =   &H00C0E6FA&
   BorderStyle     =   1  '단일 고정
   Caption         =   "CAR Line 생성 프로그램"
   ClientHeight    =   5520
   ClientLeft      =   12780
   ClientTop       =   780
   ClientWidth     =   8490
   DrawMode        =   1  '검정
   BeginProperty Font 
      Name            =   "맑은 고딕"
      Size            =   9.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "CAR_Line_Form.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5520
   ScaleWidth      =   8490
   Begin VB.FileListBox File_numbering 
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   810
      Left            =   9240
      TabIndex        =   10
      Top             =   2760
      Width           =   4335
   End
   Begin VB.TextBox Selection_View_X_Value_Text 
      BorderStyle     =   0  '없음
      Height          =   270
      Left            =   9360
      TabIndex        =   9
      Text            =   "0"
      Top             =   2280
      Width           =   855
   End
   Begin VB.TextBox Selection_View_Y_Value_Text 
      BorderStyle     =   0  '없음
      Height          =   270
      Left            =   10440
      TabIndex        =   8
      Text            =   "0"
      Top             =   2280
      Width           =   855
   End
   Begin VB.OptionButton Car_Line_Type_Option2 
      BackColor       =   &H00C0E6FA&
      Height          =   255
      Left            =   10140
      TabIndex        =   7
      Top             =   1815
      Width           =   375
   End
   Begin VB.OptionButton Car_Line_Type_Option3 
      BackColor       =   &H00C0E6FA&
      Height          =   255
      Left            =   11100
      TabIndex        =   6
      Top             =   1815
      Width           =   375
   End
   Begin VB.OptionButton Car_Line_Type_Option4 
      BackColor       =   &H00C0E6FA&
      Height          =   255
      Left            =   12060
      TabIndex        =   5
      Top             =   1815
      Width           =   375
   End
   Begin VB.OptionButton Car_Line_Type_Option1 
      Appearance      =   0  '평면
      BackColor       =   &H00C0E6FA&
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   9360
      TabIndex        =   4
      Top             =   1800
      Value           =   -1  'True
      Width           =   375
   End
   Begin VB.Frame Frame36 
      BackColor       =   &H00C0E6FA&
      Caption         =   "CarLine Option"
      Height          =   735
      Left            =   9240
      TabIndex        =   1
      Top             =   960
      Width           =   4335
      Begin VB.OptionButton CarLine_Option2 
         BackColor       =   &H00C0E6FA&
         Caption         =   "Reference"
         Height          =   255
         Left            =   2520
         TabIndex        =   3
         Top             =   360
         Width           =   1695
      End
      Begin VB.OptionButton CarLine_Option1 
         Appearance      =   0  '평면
         BackColor       =   &H00C0E6FA&
         Caption         =   "Normal "
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Value           =   -1  'True
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0E6FA&
      Caption         =   " CarLine 생성 "
      ForeColor       =   &H00333333&
      Height          =   4845
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   8295
      Begin VB.PictureBox Picture_MsgBG 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0E6FA&
         BorderStyle     =   0  '없음
         Height          =   360
         Left            =   360
         Picture         =   "CAR_Line_Form.frx":C8AA
         ScaleHeight     =   360
         ScaleWidth      =   7710
         TabIndex        =   30
         Top             =   4320
         Width           =   7710
         Begin VB.Label Message_Label 
            BackStyle       =   0  '투명
            Caption         =   "-"
            ForeColor       =   &H00333333&
            Height          =   255
            Left            =   240
            TabIndex        =   31
            Top             =   0
            Width           =   7215
         End
      End
      Begin VB.Frame Frame2 
         BackColor       =   &H00C0E6FA&
         Caption         =   " Type "
         ForeColor       =   &H00333333&
         Height          =   2715
         Left            =   120
         TabIndex        =   22
         Top             =   1560
         Width           =   3975
         Begin VB.ComboBox Car_Line_Position_Combo 
            Height          =   375
            Left            =   1560
            TabIndex        =   23
            Text            =   "T,L,H"
            Top             =   315
            Width           =   2295
         End
         Begin VB.Frame Frame13 
            BackColor       =   &H00C0E6FA&
            Height          =   1870
            Left            =   1280
            TabIndex        =   32
            Top             =   700
            Width           =   1740
            Begin VB.PictureBox TLH_50_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":159AE
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   80
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_25_5 
               BorderStyle     =   0  '없음
               Height          =   1455
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":16DB5
               ScaleHeight     =   1455
               ScaleWidth      =   1455
               TabIndex        =   79
               Top             =   240
               Width           =   1455
            End
            Begin VB.PictureBox TLH_25_10 
               BorderStyle     =   0  '없음
               Height          =   1455
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":17EE5
               ScaleHeight     =   1455
               ScaleWidth      =   1455
               TabIndex        =   78
               Top             =   240
               Width           =   1455
            End
            Begin VB.PictureBox TLH_25_15 
               BorderStyle     =   0  '없음
               Height          =   1455
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":19A92
               ScaleHeight     =   1455
               ScaleWidth      =   1455
               TabIndex        =   77
               Top             =   240
               Width           =   1455
            End
            Begin VB.PictureBox TLH_25_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":1BB66
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   76
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_50_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":1DED8
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   75
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_50_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":1FDA7
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   74
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_50_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":2233B
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   73
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_75_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":24B8F
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   72
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_75_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":263E0
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   71
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_75_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":2833F
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   70
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_75_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":2AB5D
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   69
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_100_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":2D7FF
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   68
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_100_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":2EF8D
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   67
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_100_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":30F62
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   66
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLH_100_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":335AF
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   65
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_25_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":361AC
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   64
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_25_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":37326
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   63
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_25_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":38F76
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   62
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_25_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":3B051
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   61
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_50_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":3D523
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   60
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_50_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":3E956
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   59
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_50_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":408F4
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   58
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_50_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":42E22
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   57
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_75_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":4572D
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   56
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_75_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":4727B
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   55
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_75_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":49336
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   54
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_75_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":4BD95
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   53
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_100_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":4ECFC
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   52
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_100_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":508AA
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   51
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_100_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":529F3
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   50
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox TLBLWL_100_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":55327
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   49
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_25_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":58137
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   48
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_25_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":59421
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   47
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_25_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":5AFF0
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   46
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_25_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":5D225
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   45
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_50_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":5F68E
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   44
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_50_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":60BA6
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   43
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_50_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":62ACA
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   42
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_50_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":64FFF
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   41
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_75_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":67A51
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   40
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_75_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":6941D
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   39
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_75_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":6B5CE
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   38
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_75_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":6E076
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   37
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_100_5 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":70F82
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   36
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_100_10 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":72A70
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   35
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_100_15 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":74C17
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   34
               Top             =   240
               Width           =   1500
            End
            Begin VB.PictureBox XYZ_100_20 
               BorderStyle     =   0  '없음
               Height          =   1500
               Left            =   120
               Picture         =   "CAR_Line_Form.frx":77539
               ScaleHeight     =   1500
               ScaleWidth      =   1500
               TabIndex        =   33
               Top             =   240
               Width           =   1500
            End
         End
         Begin VB.Label Label2 
            BackColor       =   &H00C0E0FF&
            BackStyle       =   0  '투명
            Caption         =   "Position :"
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00333333&
            Height          =   315
            Left            =   105
            TabIndex        =   25
            Top             =   360
            Width           =   1275
         End
         Begin VB.Label Label3 
            BackStyle       =   0  '투명
            Caption         =   "Targ :"
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00333333&
            Height          =   315
            Left            =   120
            TabIndex        =   24
            Top             =   960
            Width           =   795
         End
      End
      Begin VB.Frame Frame3 
         BackColor       =   &H00C0E6FA&
         Height          =   2745
         Left            =   4200
         TabIndex        =   13
         Top             =   1560
         Width           =   3975
         Begin VB.ComboBox Car_LineTarg_Combo 
            Height          =   375
            Left            =   1680
            TabIndex        =   17
            Text            =   "Top-Left"
            Top             =   1680
            Width           =   2175
         End
         Begin VB.ComboBox Car_Line_Space_Combo 
            Height          =   375
            Left            =   1695
            TabIndex        =   16
            Text            =   "100"
            Top             =   1200
            Width           =   2175
         End
         Begin VB.ComboBox Car_Line_Text_Size_Text 
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1680
            TabIndex        =   15
            Text            =   "5"
            Top             =   720
            Width           =   2175
         End
         Begin VB.ComboBox Car_Line_Circle_Size_Text 
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   1680
            TabIndex        =   14
            Text            =   "10"
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label Label7 
            BackStyle       =   0  '투명
            Caption         =   "Targ Position :"
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   129
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00333333&
            Height          =   315
            Left            =   120
            TabIndex        =   21
            Top             =   1740
            Width           =   1695
         End
         Begin VB.Label Label6 
            BackColor       =   &H00C0E6FA&
            Caption         =   "Space :"
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   129
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00333333&
            Height          =   315
            Left            =   120
            TabIndex        =   20
            Top             =   1260
            Width           =   795
         End
         Begin VB.Label Label5 
            BackColor       =   &H00C0E6FA&
            Caption         =   "Text Size :"
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   129
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00333333&
            Height          =   315
            Left            =   120
            TabIndex        =   19
            Top             =   705
            Width           =   1335
         End
         Begin VB.Label Label4 
            BackColor       =   &H00C0E6FA&
            Caption         =   "Circle Size :"
            BeginProperty Font 
               Name            =   "맑은 고딕"
               Size            =   9
               Charset         =   129
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00333333&
            Height          =   315
            Left            =   120
            TabIndex        =   18
            Top             =   240
            Width           =   1455
         End
      End
      Begin CAR_Line.GurhanButton CarLine_Delete 
         Height          =   495
         Left            =   240
         TabIndex        =   11
         Top             =   960
         Width           =   2175
         _ExtentX        =   3836
         _ExtentY        =   873
         Caption         =   ""
         ButtonStyle     =   4
         Picture         =   "CAR_Line_Form.frx":7A4E0
         PictureWidth    =   130
         PictureSize     =   2
         OriginalPicSizeW=   130
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "맑은 고딕"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "CAR_Line_Form.frx":7D634
         MousePointer    =   99
         ShowFocusRect   =   0   'False
         BackColor       =   12642042
      End
      Begin CAR_Line.GurhanButton Car_Line_Create_View_Selection 
         Height          =   495
         Left            =   7440
         TabIndex        =   12
         Top             =   360
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   873
         Caption         =   ""
         ButtonStyle     =   4
         Picture         =   "CAR_Line_Form.frx":7D94E
         PictureWidth    =   30
         PictureSize     =   2
         OriginalPicSizeW=   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "맑은 고딕"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "CAR_Line_Form.frx":7E522
         MousePointer    =   99
         ShowFocusRect   =   0   'False
         BackColor       =   12642042
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0E6FA&
         Caption         =   "View Selection :"
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00333333&
         Height          =   375
         Left            =   240
         TabIndex        =   27
         Top             =   480
         Width           =   1575
      End
      Begin VB.Label Car_Line_Create_View_Selection_Label 
         BackColor       =   &H00C0E6FA&
         Caption         =   "-"
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   129
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00333333&
         Height          =   255
         Left            =   2040
         TabIndex        =   26
         Top             =   480
         Width           =   4455
      End
   End
   Begin CAR_Line.GurhanButton BtEx 
      Height          =   495
      Left            =   6480
      TabIndex        =   28
      Top             =   5040
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   873
      Caption         =   ""
      ButtonStyle     =   4
      Picture         =   "CAR_Line_Form.frx":7E83C
      PictureWidth    =   120
      PictureHeight   =   29
      PictureSize     =   2
      OriginalPicSizeW=   120
      OriginalPicSizeH=   29
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "맑은 고딕"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "CAR_Line_Form.frx":81158
      MousePointer    =   99
      ShowFocusRect   =   0   'False
      BackColor       =   12642042
   End
   Begin CAR_Line.GurhanButton CarLine_Create 
      Height          =   495
      Left            =   3960
      TabIndex        =   29
      Top             =   5040
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   873
      Caption         =   ""
      ButtonStyle     =   4
      Picture         =   "CAR_Line_Form.frx":81472
      PictureWidth    =   120
      PictureHeight   =   29
      PictureSize     =   2
      OriginalPicSizeW=   120
      OriginalPicSizeH=   29
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "맑은 고딕"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "CAR_Line_Form.frx":83D8E
      MousePointer    =   99
      ShowFocusRect   =   0   'False
      BackColor       =   12642042
   End
   Begin VB.Image Ispark_Logo 
      Height          =   600
      Left            =   120
      Picture         =   "CAR_Line_Form.frx":840A8
      Top             =   4920
      Width           =   1050
   End
End
Attribute VB_Name = "CAR_Line_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------------------
' Form_Load                                                                     &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Form_Load()
'--------------------------------------------------------------------------------------------------- License_Check
Call License_Check
'--------------------------------------------------------------------------------------------------- 프로세스 없애기
Call KillProcess_Exe_Program("CAR_Line.exe")
'--------------------------------------------------------------------------------------------------- Form 위치 초기화
Call CAR_Line_Form_Inicial_Setting
'--------------------------------------------------------------------------------------------------- Error Count 초기화
Error_Count = 0
'--------------------------------------------------------------------------------------------------- CATIA 정보 가져오기
Call CATIA_Basic
'--------------------------------------------------------------------------------------------------- Open 파일 정보가 맞지 않을때.. Error 처리
If Error_Count = 1 Then
    Unload Me
End If
'--------------------------------------------------------------------------------------------------- CarLine Combo Setting
Call CarLine_Combo_Setting
'--------------------------------------------------------------------------------------------------- Message 제거
CATIA.DisplayFileAlerts = False
'--------------------------------------------------------------------------------------------------- Combo Image Setting
CAR_Line_Form.File_numbering.path = App.path & "\Targ_Image"
Call CarLine_Combo_Image_Setting
'--------------------------------------------------------------------------------------------------- Form 최상위
windows_ontop (hwnd)
'--------------------------------------------------------------------------------------------------- Main Form 위치 초기화
lWidth = Screen.Width - 10000
lHeight = 1080
Call Form_Position_Initial(CAR_Line_Form, lWidth, lHeight)

CAR_Line_Form.Message_Label = "CarLine을 생성 View 를 선택 하세요"

End Sub
'---------------------------------------------------------------------------------------------------
' 종료                                                                               &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub BtEx_Click()
Dim aaa As String
    aaa = MsgBox("종료 하겠습니다.", vbOKCancel Or vbInformation, "CATIA Automation")
    If aaa = vbOK Then
        End
    End If
End Sub
'---------------------------------------------------------------------------------------------------
' Form X 버튼  종료                                                     &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Form_Terminate()
    stopProgram
End Sub
Private Sub Form_Unload(Cancel As Integer)
    stopProgram
End Sub
Private Sub stopProgram()
    End
End Sub
'---------------------------------------------------------------------------------------------------
' 이즈파크 홈페이지 연결                                              &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Ispark_Logo_Click()
    Ispark_Homepage_Execute = ShellExecute(Me.hwnd, "Open", "http://www.ispark.kr", "", App.path, 1)
End Sub
'---------------------------------------------------------------------------------------------------
'  CAR Line 생성 View 선택                                                  &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Car_Line_Create_View_Selection_Click()
    Call Drawing_View_Selection(Car_Line_Create_View_Selection_Label)
    CAR_Line_Form.Message_Label = "View 선택 완료"
End Sub
'---------------------------------------------------------------------------------------------------
'  CAR Line 제거                                                                &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub CarLine_Delete_Click()
CAR_Line_Form.Message_Label = "CarLine 제거 중..."
    Call CATIA_Basic_Drawing
    Call Activate_View_CarLine_Delete
CAR_Line_Form.Message_Label = "CarLine 제거 완료"
End Sub
'---------------------------------------------------------------------------------------------------
'  Car_Line_Position_Combo 변경시                                    &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Car_Line_Position_Combo_Click()
    Call CarLine_Combo_Image_Setting
End Sub
'---------------------------------------------------------------------------------------------------
'  Car_Line_Circle_Size_Text 변경시                                    &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Car_Line_Circle_Size_Text_Click()
    Call CarLine_Combo_Image_Setting
End Sub
'---------------------------------------------------------------------------------------------------
'  Car_Line_Text_Size_Text 변경시                                      &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Car_Line_Text_Size_Text_Click()
    Call CarLine_Combo_Image_Setting
End Sub
'---------------------------------------------------------------------------------------------------
'  Car_Line 간격 변경시                                                       &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Car_Line_Space_Combo_Click()
    If CAR_Line_Form.Car_Line_Space_Combo.Text = "Customize" Then
        CARLine_Space_Form.Show
    End If
End Sub
'---------------------------------------------------------------------------------------------------
'  CAR Line 생성                                                                &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub CarLine_Create_Click()
If CAR_Line_Form.Car_Line_Create_View_Selection_Label.Caption = "-" Then
    Select_error = MsgBox("선택된 Drawing View가 없습니다", vbInformation, "CATIA Automation")
    Exit Sub
End If
CAR_Line_Form.Message_Label = "CarLine 범위를 지정하세요"
'--------------------------------------------------------------------------------------------------- 기 생성된 CARLine 제거
Call CATIA_Basic_Drawing
Call Activate_View_CarLine_Delete
Call CATIA_CarLine_Create(Selection_View_Object, 3)
CAR_Line_Form.Message_Label = "CarLine 생성 완료"
End Sub
'---------------------------------------------------------------------------------------------------
' License Check                                                        &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Function License_Check() As Variant
mdate = DateSerial(2019, 3, 30)
cha = DateDiff("d", Now, mdate)
    If cha < 0 Then
        aaa = MsgBox("LICENSE 사용기간이 지났습니다.", vbInformation, "ISPark CATIA Automation")
        End
    End If
End Function

