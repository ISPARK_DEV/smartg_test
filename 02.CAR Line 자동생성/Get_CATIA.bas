Attribute VB_Name = "Get_CATIA"
'--------------------------------------------------------------------------------------------------- CATIA Setting
Public CATIA, Documents, ActiveDocument, Part, Selection, HybridShapeFactory
'--------------------------------------------------------------------------------------------------- CATIA Drawing Setting
Public DrawingDocument, Drawingselection, DrawingSheets, DrawingSheet, DrawingViews, Main_View, Main_Factory2D, Back_View, Back_Factory2D
'--------------------------------------------------------------------------------------------------- Error 처리
Public Error_Count
'---------------------------------------------------------------------------------------------------
'  초기 CATIA의 실행 여부를 판단하는 함수 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function CATIA_Basic()
On Error Resume Next
    Set CATIA = GetObject(, "CATIA.Application")
    If Err.Number <> 0 Then
        MsgBox "CATIA를 먼저 실행하세요."
        End
    End If
On Error GoTo 0
Set Documents = CATIA.Documents
'--------------------------------------------------------------------------------------------------- ActiveDocument 가 없을때...
If Documents.Count = 0 Then
    Error_Message = MsgBox("Data를 Loading 하세요.", vbOKCancel Or vbInformation, "ISPark CATIA Automation")
    Error_Count = 1
    Exit Function
End If
Set ActiveDocument = CATIA.ActiveDocument
Set Selection = ActiveDocument.Selection
End Function
'---------------------------------------------------------------------------------------------------
'  CATIA_Basic_Drawing &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function CATIA_Basic_Drawing()
On Error Resume Next
    If CATIA.Documents.Count = 0 Then
    aaa = MsgBox("생성된 도면파일이 없습니다.", vbOKCancel Or vbInformation, "ISPark CATIA Automation")
        If aaa = vbOK Then
            End
        End If
    End If
    Set DrawingDocument = CATIA.ActiveDocument
    Set Drawingselection = DrawingDocument.Selection
    Set DrawingSheets = DrawingDocument.sheets
    Set DrawingSheet = DrawingSheets.ActiveSheet
    Set DrawingViews = DrawingSheet.Views
    Set Main_View = DrawingViews.Item("Main View")
    Set Main_Factory2D = Main_View.Factory2D
    Set Back_View = DrawingViews.Item("Background View")
    Set Back_Factory2D = Back_View.Factory2D
    DrawingSheet.ProjectionMethod = 1
On Error GoTo 0
End Function


