Attribute VB_Name = "Common"
'--------------------------------------------------------------------------------------------------- Directory Path
Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Const conSwNormal = 1
Public GetDirectory_value
Const HWND_TOPMOST = -1
Const HWND_NOTOPMOST = -2
Private Type BROWSEINFO '사용자 정의
    hOwner As Long
    pidlRoot As Long
    pszDisplayName As String
    lpszTitle As String
    ulFlags As Long
    lpfn As Long
    lParam As Long
    iImage As Long
End Type
'32비트 API함수 선언
Private Declare Function SHGetPathFromIDList Lib "shell32.dll" _
Alias "SHGetPathFromIDListA" (ByVal pidl As Long, ByVal pszPath As String) As Long
Private Declare Function SHBrowseForFolder Lib "shell32.dll" _
Alias "SHBrowseForFolderA" (lpBrowseInfo As BROWSEINFO) As Long
Private Declare Function SetWindowPos Lib "user32" ( _
    ByVal hwnd As Long, _
    ByVal hwndInsertAfter As Long, _
    ByVal X As Long, _
    ByVal Y As Long, _
    ByVal cx As Long, _
    ByVal cy As Long, _
    ByVal wFlags As Long _
) As Long
Public Sub windows_ontop(ByVal window As Long)
    SetWindowPos window, HWND_TOPMOST, 0, 0, 0, 0, &H2 Or &H1
End Sub
Public Sub windows_not_ontop(ByVal window As Long)
    SetWindowPos window, HWND_NOTOPMOST, 0, 0, 0, 0, &H2 Or &H1
End Sub
'--- 경로 도우미
Public Function GetDirectory(Optional strMsg) As String
    Dim bInfo As BROWSEINFO
    Dim path As String
    Dim r As Long, X As Long, pos As Integer
    bInfo.pidlRoot = 0&
    If IsMissing(strMsg) Then
        bInfo.lpszTitle = "폴더를 선택하세요"
    Else
        bInfo.lpszTitle = strMsg
    End If
    bInfo.ulFlags = &H1
    X = SHBrowseForFolder(bInfo)
    path = Space$(512)
    r = SHGetPathFromIDList(ByVal X, ByVal path)
    If r Then
        pos = InStr(path, Chr$(0))
        GetDirectory = Left(path, pos - 1)
        GetDirectory_value = GetDirectory
    Else
        GetDirectory = ""
    End If
End Function
'---------------------------------------------------------------------------------------------------
' Form Position                                                         &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function Form_Position_Initial(Form_Name, lWidth, lHeight)
On Error Resume Next
    Form_Name.Left = lWidth
    Form_Name.Top = lHeight
On Error GoTo 0
End Function
'---------------------------------------------------------------------------------------------------
' Delay  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub Delay(second As Double)
Dim LTime
LTime = Timer
While Timer - LTime < second
    DoEvents
Wend
End Sub
'---------------------------------------------------------------------------------------------------
' KillProcess_Exe_Program &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub KillProcess_Exe_Program(Program_Exe_Name)
On Error Resume Next
    Dim pgm As String
    Dim wmi As Object
    Dim processes, process
    Dim sQuery As String
    '--------------------------------------------------------------------------------------------------- Excel Close
    Delay (0.5)
    pgm = Program_Exe_Name
    Set wmi = GetObject("winmgmts:")
    sQuery = "select * from win32_process where name='" & pgm & "'"
    Set processes = wmi.execquery(sQuery)
    If processes.Count = 2 Then
        For Each process In processes
            process.Terminate
            Exit For
        Next
        Set wmi = Nothing
    End If
On Error GoTo 0
End Sub

'---------------------------------------------------------------------------------------------------
' 초기 Form 위치 Setting                                             &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub CAR_Line_Form_Inicial_Setting()
    CAR_Line_Form.Frame1.Left = 10 * Screen.TwipsPerPixelX
    CAR_Line_Form.Frame1.Top = 8 * Screen.TwipsPerPixelY
    CAR_Line_Form.Frame1.Width = 545 * Screen.TwipsPerPixelX
    CAR_Line_Form.Frame1.Height = 319 * Screen.TwipsPerPixelY
    
    CAR_Line_Form.Label1.Left = 26 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left
    CAR_Line_Form.Label1.Top = 42 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    CAR_Line_Form.Car_Line_Create_View_Selection_Label.Left = CAR_Line_Form.Label1.Left + CAR_Line_Form.Label1.Width + 10 * Screen.TwipsPerPixelX
    CAR_Line_Form.Car_Line_Create_View_Selection_Label.Top = 42 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    
    CAR_Line_Form.Car_Line_Create_View_Selection.Left = 506 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left
    CAR_Line_Form.Car_Line_Create_View_Selection.Top = 30 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    CAR_Line_Form.Car_Line_Create_View_Selection.Width = 32 * Screen.TwipsPerPixelX
    CAR_Line_Form.Car_Line_Create_View_Selection.Height = 34 * Screen.TwipsPerPixelY
    
    CAR_Line_Form.CarLine_Delete.Left = 24 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left
    CAR_Line_Form.CarLine_Delete.Top = 66 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    CAR_Line_Form.CarLine_Delete.Width = 132 * Screen.TwipsPerPixelX
    CAR_Line_Form.CarLine_Delete.Height = 34 * Screen.TwipsPerPixelY

    CAR_Line_Form.Frame2.Left = 26 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left
    CAR_Line_Form.Frame2.Top = 108 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    CAR_Line_Form.Frame2.Width = 251 * Screen.TwipsPerPixelX
    CAR_Line_Form.Frame2.Height = 176 * Screen.TwipsPerPixelY
    
    CAR_Line_Form.Label2.Left = 42 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame2.Left
    CAR_Line_Form.Label2.Top = 140 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    
    CAR_Line_Form.Car_Line_Position_Combo.Left = 111 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame2.Left
    CAR_Line_Form.Car_Line_Position_Combo.Top = 135 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    CAR_Line_Form.Car_Line_Position_Combo.Width = 108 * Screen.TwipsPerPixelX
    'Car_Line_Position_Combo.Height = 24 * Screen.TwipsPerPixelY
    
    CAR_Line_Form.Label3.Left = 42 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame2.Left
    CAR_Line_Form.Label3.Top = 176 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    
    CAR_Line_Form.Frame3.Left = 288 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left
    CAR_Line_Form.Frame3.Top = 108 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    CAR_Line_Form.Frame3.Width = 251 * Screen.TwipsPerPixelX
    CAR_Line_Form.Frame3.Height = 176 * Screen.TwipsPerPixelY
        
    CAR_Line_Form.Label4.Left = 304 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Label4.Top = 140 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame3.Top
    
    CAR_Line_Form.Label5.Left = 304 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Label5.Top = 176 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame3.Top
    
    CAR_Line_Form.Label6.Left = 304 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Label6.Top = 212 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame3.Top
    
    CAR_Line_Form.Label7.Left = 304 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Label7.Top = 248 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame3.Top
        
    CAR_Line_Form.Car_Line_Circle_Size_Text.Left = 392 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Car_Line_Circle_Size_Text.Top = 135 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    CAR_Line_Form.Car_Line_Circle_Size_Text.Width = 136 * Screen.TwipsPerPixelX
    
    CAR_Line_Form.Car_Line_Text_Size_Text.Left = 392 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Car_Line_Text_Size_Text.Top = 171 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    CAR_Line_Form.Car_Line_Text_Size_Text.Width = 136 * Screen.TwipsPerPixelX
    
    CAR_Line_Form.Car_Line_Space_Combo.Left = 392 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Car_Line_Space_Combo.Top = 208 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    CAR_Line_Form.Car_Line_Space_Combo.Width = 136 * Screen.TwipsPerPixelX
    
    CAR_Line_Form.Car_LineTarg_Combo.Left = 392 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left - CAR_Line_Form.Frame3.Left
    CAR_Line_Form.Car_LineTarg_Combo.Top = 244 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top - CAR_Line_Form.Frame2.Top
    CAR_Line_Form.Car_LineTarg_Combo.Width = 136 * Screen.TwipsPerPixelX
    
    CAR_Line_Form.CarLine_Create.Left = 309 * Screen.TwipsPerPixelX
    CAR_Line_Form.CarLine_Create.Top = 332 * Screen.TwipsPerPixelY
    CAR_Line_Form.CarLine_Create.Width = 122 * Screen.TwipsPerPixelX
    CAR_Line_Form.CarLine_Create.Height = 31 * Screen.TwipsPerPixelY
    
    CAR_Line_Form.BtEx.Left = 434 * Screen.TwipsPerPixelX
    CAR_Line_Form.BtEx.Top = 332 * Screen.TwipsPerPixelY
    CAR_Line_Form.BtEx.Width = 122 * Screen.TwipsPerPixelX
    CAR_Line_Form.BtEx.Height = 31 * Screen.TwipsPerPixelY
        
    CAR_Line_Form.Picture_MsgBG.Left = 26 * Screen.TwipsPerPixelX - CAR_Line_Form.Frame1.Left
    CAR_Line_Form.Picture_MsgBG.Top = 292 * Screen.TwipsPerPixelY - CAR_Line_Form.Frame1.Top
    CAR_Line_Form.Message_Label.Left = 10 * Screen.TwipsPerPixelX
    CAR_Line_Form.Message_Label.Top = 4 * Screen.TwipsPerPixelY
    CAR_Line_Form.Message_Label.FontSize = 9
    
    CAR_Line_Form.Ispark_Logo.Left = 10 * Screen.TwipsPerPixelX
    CAR_Line_Form.Ispark_Logo.Top = 328 * Screen.TwipsPerPixelY
End Sub
'---------------------------------------------------------------------------------------------------
' 초기 Form 위치 Setting                                             &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub CARLine_Space_Form_Inicial_Setting()
    CARLine_Space_Form.Frame1.Left = 10 * Screen.TwipsPerPixelX
    CARLine_Space_Form.Frame1.Top = 8 * Screen.TwipsPerPixelY
    CARLine_Space_Form.Frame1.Width = 370 * Screen.TwipsPerPixelX
    CARLine_Space_Form.Frame1.Height = 99 * Screen.TwipsPerPixelY
    
    CARLine_Space_Form.Label1.Left = 26 * Screen.TwipsPerPixelX - CARLine_Space_Form.Frame1.Left
    CARLine_Space_Form.Label1.Top = 42 * Screen.TwipsPerPixelY - CARLine_Space_Form.Frame1.Top
        
    CARLine_Space_Form.CarLine_Text.Left = 139 * Screen.TwipsPerPixelX - CARLine_Space_Form.Frame1.Left
    CARLine_Space_Form.CarLine_Text.Top = 36 * Screen.TwipsPerPixelY - CARLine_Space_Form.Frame1.Top
    CARLine_Space_Form.CarLine_Text.Width = 108 * Screen.TwipsPerPixelX
    CARLine_Space_Form.CarLine_Text.Height = 24 * Screen.TwipsPerPixelY
    
    CARLine_Space_Form.CarLine_Value_Create.Left = 134 * Screen.TwipsPerPixelX
    CARLine_Space_Form.CarLine_Value_Create.Top = 112 * Screen.TwipsPerPixelY
    CARLine_Space_Form.CarLine_Value_Create.Width = 122 * Screen.TwipsPerPixelX
    CARLine_Space_Form.CarLine_Value_Create.Height = 31 * Screen.TwipsPerPixelY
    
    CARLine_Space_Form.BtEx.Left = 259 * Screen.TwipsPerPixelX
    CARLine_Space_Form.BtEx.Top = 112 * Screen.TwipsPerPixelY
    CARLine_Space_Form.BtEx.Width = 122 * Screen.TwipsPerPixelX
    CARLine_Space_Form.BtEx.Height = 31 * Screen.TwipsPerPixelY
    
    CARLine_Space_Form.Picture_MsgBG.Left = 26 * Screen.TwipsPerPixelX - CARLine_Space_Form.Frame1.Left
    CARLine_Space_Form.Picture_MsgBG.Top = 72 * Screen.TwipsPerPixelY - CARLine_Space_Form.Frame1.Top
    CARLine_Space_Form.Message_Label.Left = 10 * Screen.TwipsPerPixelX
    CARLine_Space_Form.Message_Label.Top = 4 * Screen.TwipsPerPixelY
    CARLine_Space_Form.Message_Label.FontSize = 9
    
    CARLine_Space_Form.Ispark_Logo.Left = 10 * Screen.TwipsPerPixelX
    CARLine_Space_Form.Ispark_Logo.Top = 108 * Screen.TwipsPerPixelY

End Sub



