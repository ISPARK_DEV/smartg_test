VERSION 5.00
Begin VB.Form CARLine_Space_Form 
   BackColor       =   &H00C0E6FA&
   BorderStyle     =   1  '단일 고정
   Caption         =   "CAR Line 간격 조정"
   ClientHeight    =   2220
   ClientLeft      =   12780
   ClientTop       =   795
   ClientWidth     =   5865
   BeginProperty Font 
      Name            =   "맑은 고딕"
      Size            =   9
      Charset         =   129
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "CARLine_Space_Form.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   5865
   Begin VB.PictureBox Ispark_Logo 
      Appearance      =   0  '평면
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  '없음
      ForeColor       =   &H80000008&
      Height          =   600
      Left            =   120
      Picture         =   "CARLine_Space_Form.frx":C8AA
      ScaleHeight     =   600
      ScaleWidth      =   1050
      TabIndex        =   2
      Top             =   1440
      Width           =   1050
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0E6FA&
      Caption         =   " CarLine 간격 조정 "
      BeginProperty Font 
         Name            =   "맑은 고딕"
         Size            =   9.75
         Charset         =   129
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00333333&
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5655
      Begin VB.PictureBox Picture_MsgBG 
         AutoSize        =   -1  'True
         BackColor       =   &H00C0E6FA&
         BorderStyle     =   0  '없음
         Height          =   360
         Left            =   240
         Picture         =   "CARLine_Space_Form.frx":EA0E
         ScaleHeight     =   360
         ScaleWidth      =   5085
         TabIndex        =   6
         Top             =   840
         Width           =   5085
         Begin VB.Label Message_Label 
            BackStyle       =   0  '투명
            Caption         =   "-"
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   120
            Width           =   4575
         End
      End
      Begin VB.TextBox CarLine_Text 
         Height          =   375
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0E6FA&
         Caption         =   "CarLine 간격 입력 : "
         ForeColor       =   &H00333333&
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   1815
      End
   End
   Begin CAR_Line.GurhanButton CarLine_Value_Create 
      Height          =   495
      Left            =   1680
      TabIndex        =   4
      Top             =   1560
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   873
      Caption         =   ""
      ButtonStyle     =   4
      Picture         =   "CARLine_Space_Form.frx":149F2
      PictureWidth    =   120
      PictureHeight   =   29
      PictureSize     =   2
      OriginalPicSizeW=   120
      OriginalPicSizeH=   29
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "맑은 고딕"
         Size            =   9
         Charset         =   129
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "CARLine_Space_Form.frx":1730E
      MousePointer    =   99
      ShowFocusRect   =   0   'False
      BackColor       =   12642042
   End
   Begin CAR_Line.GurhanButton BtEx 
      Height          =   495
      Left            =   3840
      TabIndex        =   5
      Top             =   1560
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   873
      Caption         =   ""
      ButtonStyle     =   4
      Picture         =   "CARLine_Space_Form.frx":17628
      PictureWidth    =   120
      PictureHeight   =   29
      PictureSize     =   2
      OriginalPicSizeW=   120
      OriginalPicSizeH=   29
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "맑은 고딕"
         Size            =   9
         Charset         =   129
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MouseIcon       =   "CARLine_Space_Form.frx":19F44
      MousePointer    =   99
      ShowFocusRect   =   0   'False
      BackColor       =   12642042
   End
End
Attribute VB_Name = "CARLine_Space_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------------------
' Form_Load                                                                     &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Form_Load()
'--------------------------------------------------------------------------------------------------- Form 위치 초기화
Call CARLine_Space_Form_Inicial_Setting
'--------------------------------------------------------------------------------------------------- Form 최상위
windows_ontop (hwnd)
'--------------------------------------------------------------------------------------------------- Main Form 위치 초기화
lWidth = Screen.Width - 10000
lHeight = 1080
Call Form_Position_Initial(CARLine_Space_Form, lWidth, lHeight)
End Sub
'---------------------------------------------------------------------------------------------------
'  CAR Line 간격 적용                                                         &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub CarLine_Value_Create_Click()
'--------------------------------------------------------------------------------------------------- 값이 없을때...
If CarLine_Text.Text = "" Then
    Select_error = MsgBox("CAR Line 생성 간격을 입력하세요", vbInformation, "CATIA Automation")
    Exit Sub
Else
    '--------------------------------------------------------------------------------------------------- 값 입력
    CAR_Line_Form.Car_Line_Space_Combo.Text = CARLine_Space_Form.CarLine_Text.Text
    Message_Label.Caption = "CAR Line 간격이 적용 되었습니다"
End If
End Sub
'---------------------------------------------------------------------------------------------------
'  종료                                                                              &&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub BtEx_Click()
    CARLine_Space_Form.Hide
End Sub



