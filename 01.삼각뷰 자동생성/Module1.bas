Attribute VB_Name = "Common"
'--------------------------------------------------------------------------------------------------- Directory Path
Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Public Const conSwNormal = 1
Public GetDirectory_value
Const HWND_TOPMOST = -1
Const HWND_NOTOPMOST = -2
Private Type BROWSEINFO '사용자 정의
    hOwner As Long
    pidlRoot As Long
    pszDisplayName As String
    lpszTitle As String
    ulFlags As Long
    lpfn As Long
    lParam As Long
    iImage As Long
End Type
'32비트 API함수 선언
Private Declare Function SHGetPathFromIDList Lib "shell32.dll" _
Alias "SHGetPathFromIDListA" (ByVal pidl As Long, ByVal pszPath As String) As Long
Private Declare Function SHBrowseForFolder Lib "shell32.dll" _
Alias "SHBrowseForFolderA" (lpBrowseInfo As BROWSEINFO) As Long
Private Declare Function SetWindowPos Lib "user32" ( _
    ByVal hwnd As Long, _
    ByVal hwndInsertAfter As Long, _
    ByVal X As Long, _
    ByVal Y As Long, _
    ByVal cx As Long, _
    ByVal cy As Long, _
    ByVal wFlags As Long _
) As Long
Public Sub windows_ontop(ByVal window As Long)
    SetWindowPos window, HWND_TOPMOST, 0, 0, 0, 0, &H2 Or &H1
End Sub
Public Sub windows_not_ontop(ByVal window As Long)
    SetWindowPos window, HWND_NOTOPMOST, 0, 0, 0, 0, &H2 Or &H1
End Sub
'---------------------------------------------------------------------------------------------------
' 폴더 경로 선택                                                          &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function GetDirectory(Optional strMsg) As String
    Dim bInfo As BROWSEINFO
    Dim path As String
    Dim r As Long, X As Long, pos As Integer
    bInfo.pidlRoot = 0&
    If IsMissing(strMsg) Then
        bInfo.lpszTitle = "폴더를 선택하세요"
    Else
        bInfo.lpszTitle = strMsg
    End If
    bInfo.ulFlags = &H1
    X = SHBrowseForFolder(bInfo)
    path = Space$(512)
    r = SHGetPathFromIDList(ByVal X, ByVal path)
    If r Then
        pos = InStr(path, Chr$(0))
        GetDirectory = Left(path, pos - 1)
        GetDirectory_value = GetDirectory
    Else
        GetDirectory = ""
    End If
End Function
'---------------------------------------------------------------------------------------------------
' Form Position                                                         &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function Form_Position_Initial(Form_Name, lWidth, lHeight)
On Error Resume Next
    Form_Name.Left = lWidth
    Form_Name.Top = lHeight
On Error GoTo 0
End Function
'---------------------------------------------------------------------------------------------------
' Delay                                                                     &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub Delay(second As Double)
Dim LTime
LTime = Timer
While Timer - LTime < second
    DoEvents
Wend
End Sub
'---------------------------------------------------------------------------------------------------
' KillProcess_Exe_Program &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub KillProcess_Exe_Program(Program_Exe_Name)
On Error Resume Next
    Dim pgm As String
    Dim wmi As Object
    Dim processes, process
    Dim sQuery As String
    '--------------------------------------------------------------------------------------------------- Excel Close
    Delay (0.5)
    pgm = Program_Exe_Name
    Set wmi = GetObject("winmgmts:")
    sQuery = "select * from win32_process where name='" & pgm & "'"
    Set processes = wmi.execquery(sQuery)
    If processes.Count = 2 Then
        For Each process In processes
            process.Terminate
            Exit For
        Next
        Set wmi = Nothing
    End If
On Error GoTo 0
End Sub
'---------------------------------------------------------------------------------------------------
' 초기 Form 위치 Setting                                             &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Sub Program_Form_Inicial_Setting()
    Trigonometry_Form.Frame1.Left = 10 * Screen.TwipsPerPixelX
    Trigonometry_Form.Frame1.Top = 8 * Screen.TwipsPerPixelY
    Trigonometry_Form.Frame1.Width = 490 * Screen.TwipsPerPixelX
    Trigonometry_Form.Frame1.Height = 171 * Screen.TwipsPerPixelY
    
    Trigonometry_Form.Label1.Left = 26 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Label1.Top = 42 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.Label2.Left = 26 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Label2.Top = 78 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.Label3.Left = 26 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Label3.Top = 114 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    
    Trigonometry_Form.Plane_Selection.Left = 456 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Plane_Selection.Top = 104 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.Plane_Selection.Width = 32 * Screen.TwipsPerPixelX
    Trigonometry_Form.Plane_Selection.Height = 34 * Screen.TwipsPerPixelY
    
    Trigonometry_Form.View_Scale_Text.Left = 102 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.View_Scale_Text.Top = 36 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.View_Scale_Text.Width = 108 * Screen.TwipsPerPixelX
    Trigonometry_Form.View_Scale_Text.Height = 24 * Screen.TwipsPerPixelY
    
    Trigonometry_Form.Projection_Combo.Left = 102 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Projection_Combo.Top = 72 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.Projection_Combo.Width = 108 * Screen.TwipsPerPixelX
    
    Trigonometry_Form.Plane_Selection_Text.Left = 102 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Plane_Selection_Text.Top = 108 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.Plane_Selection_Text.Width = 350 * Screen.TwipsPerPixelX
    Trigonometry_Form.Plane_Selection_Text.Height = 24 * Screen.TwipsPerPixelY
    
    Trigonometry_Form.View_Create.Left = 254 * Screen.TwipsPerPixelX
    Trigonometry_Form.View_Create.Top = 184 * Screen.TwipsPerPixelY
    Trigonometry_Form.View_Create.Width = 122 * Screen.TwipsPerPixelX
    Trigonometry_Form.View_Create.Height = 31 * Screen.TwipsPerPixelY
    
    Trigonometry_Form.BtEx.Left = 379 * Screen.TwipsPerPixelX
    Trigonometry_Form.BtEx.Top = 184 * Screen.TwipsPerPixelY
    Trigonometry_Form.BtEx.Width = 122 * Screen.TwipsPerPixelX
    Trigonometry_Form.BtEx.Height = 31 * Screen.TwipsPerPixelY
    
    Trigonometry_Form.Picture_MsgBG.Left = 26 * Screen.TwipsPerPixelX - Trigonometry_Form.Frame1.Left
    Trigonometry_Form.Picture_MsgBG.Top = 144 * Screen.TwipsPerPixelY - Trigonometry_Form.Frame1.Top
    Trigonometry_Form.Message_Label.Left = 10 * Screen.TwipsPerPixelX
    Trigonometry_Form.Message_Label.Top = 4 * Screen.TwipsPerPixelY
    Trigonometry_Form.Message_Label.FontSize = 9
    
    Trigonometry_Form.Label1.FontSize = 9
    Trigonometry_Form.Label2.FontSize = 9
    Trigonometry_Form.Label3.FontSize = 9
    
    Trigonometry_Form.Ispark_Logo.Left = 10 * Screen.TwipsPerPixelX
    Trigonometry_Form.Ispark_Logo.Top = 180 * Screen.TwipsPerPixelY
End Sub

