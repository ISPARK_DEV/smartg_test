Attribute VB_Name = "Trigonometry_Module"
'--------------------------------------------------------------------------------------------------- Plane 가져오기
Public Plane_Components(11)
'--------------------------------------------------------------------------------------------------- 도면 Size 가져오기
Public View_Size_01(4), View_Size_02(4), View_Size_03(4)
'---------------------------------------------------------------------------------------------------
'  Projection_Combo 초기값 정의            &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function Projection_Combo_Inincial_Setting()
Trigonometry_Form.Projection_Combo.AddItem "First Angle"
Trigonometry_Form.Projection_Combo.AddItem "Third Angle"
Trigonometry_Form.Projection_Combo.Text = "Third Angle"
End Function
'---------------------------------------------------------------------------------------------------
'  Front_Plane 선택                              &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function Front_Plane_Selection()
Call CATIA_Basic
If Selection.Count = "0" Then
     Select_error = MsgBox("선택된 Plane이 없습니다", vbInformation, "ISPark CATIA Automation")
     Error_Count = 1
     Exit Function
End If
'--------------------------------------------------------------------------------------------------- 화면에서 Plane 선택
Dim InputObjectType(0)
Trigonometry_Form.Message_Label = "Plane을 선택하세요"
InputObjectType(0) = "Plane"
Status = Selection.SelectElement2(InputObjectType, "Select a Plane", True)
Set Selection_Plane = Selection.Item(1).Value
Trigonometry_Form.Plane_Selection_Text.Text = Selection.Item(1).Value.Name
Trigonometry_Form.Message_Label = "-"
'--------------------------------------------------------------------------------------------------- Plane 정보 가져오기
If Right(CATIA.ActiveDocument.Name, 7) = "CATPart" Then
    '--------------------------------------------------------------------------------------------------- Part 일때...
    Set TheSPAWorkbench = CATIA.ActiveDocument.GetWorkbench("SPAWorkbench")
    Set Plane_Object_value = CATIA.ActiveDocument.Part.CreateReferenceFromObject(Selection_Plane)
    Set TheMeasurable = TheSPAWorkbench.GetMeasurable(Plane_Object_value)
    TheMeasurable.GetPlane Plane_Components
Else
    '--------------------------------------------------------------------------------------------------- Product 일때...
    Set TheSPAWorkbench = CATIA.ActiveDocument.GetWorkbench("SPAWorkbench")
    '--------------------------------------------------------------------------------------------------- Part 찾기
    Set Part = Selection_Plane.Parent
    For i = 1 To 20
        If Right(Part.Name, 7) = "CATPart" Then
            Set Part = Part.Part
            Exit For
        End If
        Set Part = Part.Parent
    Next
    Set Plane_Object_value = Part.CreateReferenceFromObject(Selection_Plane)
    Set TheMeasurable = TheSPAWorkbench.GetMeasurable(Plane_Object_value)
    TheMeasurable.GetPlane Plane_Components
End If
End Function
'---------------------------------------------------------------------------------------------------
'  도면 View Text 제거                           &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Public Function View_Name_Text_Delete(Delete_Text_View)
On Error Resume Next
    '--------------------------------------------------------------------------------------------------- Text 제거
    Drawingselection.Clear
    Drawingselection.Add Delete_Text_View.Texts.Item(1)
    Drawingselection.Delete
On Error GoTo 0
End Function





