VERSION 5.00
Begin VB.Form Trigonometry_Form 
   Appearance      =   0  '평면
   BackColor       =   &H00C0E6FA&
   BorderStyle     =   1  '단일 고정
   Caption         =   "도면 삼각뷰 생성 프로그램"
   ClientHeight    =   3300
   ClientLeft      =   12780
   ClientTop       =   780
   ClientWidth     =   7665
   DrawMode        =   1  '검정
   BeginProperty Font 
      Name            =   "맑은 고딕"
      Size            =   9.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Trigonometry_Form.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3300
   ScaleWidth      =   7665
   Begin VB.Frame Frame1 
      BackColor       =   &H00C0E6FA&
      Caption         =   " 도면 생성 "
      ForeColor       =   &H00333333&
      Height          =   2355
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7455
      Begin Trigonometry.GurhanButton Plane_Selection 
         Height          =   495
         Left            =   6720
         TabIndex        =   6
         Top             =   1080
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   873
         Caption         =   ""
         ButtonStyle     =   4
         Picture         =   "Trigonometry_Form.frx":C8AA
         PictureWidth    =   30
         PictureSize     =   2
         OriginalPicSizeW=   30
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "맑은 고딕"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         MouseIcon       =   "Trigonometry_Form.frx":D47E
         MousePointer    =   99
         ShowFocusRect   =   0   'False
         BackColor       =   12642042
      End
      Begin VB.TextBox View_Scale_Text 
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1800
         TabIndex        =   5
         Text            =   "1"
         Top             =   330
         Width           =   975
      End
      Begin VB.ComboBox Projection_Combo 
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Left            =   1800
         TabIndex        =   4
         Text            =   "First Angle"
         Top             =   795
         Width           =   1815
      End
      Begin VB.TextBox Plane_Selection_Text 
         Height          =   375
         Left            =   1800
         TabIndex        =   1
         Top             =   1200
         Width           =   4815
      End
      Begin VB.PictureBox Picture_MsgBG 
         AutoSize        =   -1  'True
         BorderStyle     =   0  '없음
         Height          =   360
         Left            =   240
         Picture         =   "Trigonometry_Form.frx":D798
         ScaleHeight     =   360
         ScaleWidth      =   6885
         TabIndex        =   10
         Top             =   1800
         Width           =   6885
         Begin VB.Label Message_Label 
            BackStyle       =   0  '투명
            Caption         =   "-"
            ForeColor       =   &H00333333&
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   0
            Width           =   6615
         End
      End
      Begin VB.Label Label3 
         BackColor       =   &H00C0E6FA&
         Caption         =   "Plane 선택 :"
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   129
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00333333&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label Label2 
         BackColor       =   &H00C0E6FA&
         Caption         =   "투영법 선택 :"
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   129
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00333333&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   840
         Width           =   1215
      End
      Begin VB.Label Label1 
         BackColor       =   &H00C0E6FA&
         Caption         =   "View Scale :"
         BeginProperty Font 
            Name            =   "맑은 고딕"
            Size            =   9
            Charset         =   129
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00333333&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   360
         Width           =   1215
      End
   End
   Begin Trigonometry.GurhanButton BtEx 
      Height          =   495
      Left            =   5640
      TabIndex        =   8
      Top             =   2520
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   873
      Caption         =   ""
      ButtonStyle     =   4
      Picture         =   "Trigonometry_Form.frx":1593C
      PictureWidth    =   120
      PictureHeight   =   29
      PictureSize     =   2
      OriginalPicSizeW=   120
      OriginalPicSizeH=   29
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "맑은 고딕"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   99
      ShowFocusRect   =   0   'False
      XPDefaultColors =   0   'False
      BackColor       =   12642042
   End
   Begin Trigonometry.GurhanButton View_Create 
      Height          =   495
      Left            =   3480
      TabIndex        =   9
      Top             =   2520
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   873
      Caption         =   ""
      ButtonStyle     =   4
      Picture         =   "Trigonometry_Form.frx":18258
      PictureWidth    =   120
      PictureHeight   =   29
      PictureSize     =   2
      OriginalPicSizeW=   120
      OriginalPicSizeH=   29
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "맑은 고딕"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      MousePointer    =   99
      ShowFocusRect   =   0   'False
      BackColor       =   12642042
   End
   Begin VB.Image Ispark_Logo 
      Height          =   600
      Left            =   120
      Picture         =   "Trigonometry_Form.frx":1AB74
      Top             =   2520
      Width           =   1050
   End
End
Attribute VB_Name = "Trigonometry_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------------------
'  Form_Load                                                            &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Form_Load()
'--------------------------------------------------------------------------------------------------- License_Check
Call License_Check
'--------------------------------------------------------------------------------------------------- 프로세스 없애기
Call KillProcess_Exe_Program("Trigonometry_Create_Program.exe")
'--------------------------------------------------------------------------------------------------- Form Setting 초기화
Call Program_Form_Inicial_Setting
'--------------------------------------------------------------------------------------------------- Form 최상위
windows_ontop (hwnd)
'--------------------------------------------------------------------------------------------------- Error Count 초기화
Error_Count = 0
'--------------------------------------------------------------------------------------------------- CATIA 정보 가져오기
Call CATIA_Basic
'--------------------------------------------------------------------------------------------------- Open 파일 정보가 맞지 않을때.. Error 처리
If Error_Count = 1 Then
    End
End If
'--------------------------------------------------------------------------------------------------- Projection_Combo 초기값 정의
Call Projection_Combo_Inincial_Setting
'--------------------------------------------------------------------------------------------------- Main Form 위치 초기화
lWidth = Screen.Width - 10000
lHeight = 1080
Call Form_Position_Initial(Trigonometry_Form, lWidth, lHeight)
'--------------------------------------------------------------------------------------------------- CATIA 확인창 없애기
CATIA.DisplayFileAlerts = False
End Sub
'---------------------------------------------------------------------------------------------------
' 이즈파크 홈페이지 연결                                              &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Ispark_Logo_Click()
    Ispark_Homepage_Execute = ShellExecute(Me.hwnd, "Open", "http://www.ispark.kr", "", App.path, 1)
End Sub
'---------------------------------------------------------------------------------------------------
' 종료                                                                       &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub BtEx_Click()
Dim aaa As String
    aaa = MsgBox("종료 하겠습니다.", vbOKCancel Or vbInformation, "CATIA Automation")
    If aaa = vbOK Then
        End
    End If
End Sub
'---------------------------------------------------------------------------------------------------
' Form X 버튼  종료                                                     &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Form_Terminate()
    stopProgram
End Sub
Private Sub Form_Unload(Cancel As Integer)
    stopProgram
End Sub
Private Sub stopProgram()
    End
End Sub
'---------------------------------------------------------------------------------------------------
' Plane 선택                                                               &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub Plane_Selection_Click()
'--------------------------------------------------------------------------------------------------- Plane 선택
Call Front_Plane_Selection
'--------------------------------------------------------------------------------------------------- Open 파일 정보가 맞지 않을때.. Error 처리
If Error_Count = 1 Then
    Exit Sub
End If
End Sub
'---------------------------------------------------------------------------------------------------
'  View 생성                                                      &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Sub View_Create_Click()
'--------------------------------------------------------------------------------------------------- 선택한 Plane가 없을때...
If Plane_Selection_Text.Text = "" Then
    Error_Message = MsgBox("Plane을 선택하세요.", vbOKCancel Or vbInformation, "ISPark CATIA Automation")
    Exit Sub
End If
Message_Label.Caption = "View 생성 중..."
'--------------------------------------------------------------------------------------------------- ActiveDocument 가 Product 일때...
If Right(CATIA.ActiveDocument.FullName, 10) = "CATProduct" Then
    '--------------------------------------------------------------------------------------------------- 생성 Product & Part 정의
    Set Work_Data = CATIA.ActiveDocument.Product
Else
    Set Work_Data = CATIA.ActiveDocument.Part
End If
'--------------------------------------------------------------------------------------------------- Sheet 생성 ( Drawing File 생성 )
Set Drawing_Documents = CATIA.Documents
Set Drawing_Documents_Add = Drawing_Documents.Add("Drawing")
'--------------------------------------------------------------------------------------------------- 도면 기본 정보 가져오기
Call CATIA_Basic_Drawing
'--------------------------------------------------------------------------------------------------- First Angle & Third Angle 선택
If Projection_Combo.Text = "First Angle" Then
    DrawingSheet.ProjectionMethod = 0
Else
    DrawingSheet.ProjectionMethod = 1
End If
'---------------------------------------------------------------------------------------------------
'--------------------------------------------------------------------------------------------------- Front View 생성
'---------------------------------------------------------------------------------------------------
Set DrawingView = DrawingViews.Add("AutomaticNaming")
Set Factory2D = DrawingView.Factory2D
Set DrawingViewGenerativeLinks = DrawingView.GenerativeLinks
Set DrawingViewGenerativeBehavior = DrawingView.GenerativeBehavior
'--------------------------------------------------------------------------------------------------- 도면 생성 Part 선택
DrawingViewGenerativeLinks.AddLink Work_Data
DrawingViewGenerativeBehavior.DefineFrontView Plane_Components(3), Plane_Components(4), Plane_Components(5), Plane_Components(6), Plane_Components(7), Plane_Components(8)
DrawingView.X = 0
DrawingView.Y = 0
DrawingView.Scale2 = Trigonometry_Form.View_Scale_Text
Set DrawingViewGenerativeBehavior = DrawingView.GenerativeBehavior
DrawingViewGenerativeBehavior.Update
DrawingView.Activate
'--------------------------------------------------------------------------------------------------- Text 제거
Call View_Name_Text_Delete(DrawingView)
'---------------------------------------------------------------------------------------------------
'--------------------------------------------------------------------------------------------------- Right View 생성
'---------------------------------------------------------------------------------------------------
Set DrawingViewGenerativeBehavior = DrawingView.GenerativeBehavior
Set drawingView2 = DrawingViews.Add("AutomaticNaming")
Set drawingViewGenerativeBehavior2 = drawingView2.GenerativeBehavior
drawingViewGenerativeBehavior2.DefineProjectionView DrawingViewGenerativeBehavior, 0
Set drawingViewGenerativeLinks1 = drawingView2.GenerativeLinks
Set drawingViewGenerativeLinks2 = DrawingView.GenerativeLinks
drawingViewGenerativeLinks2.CopyLinksTo drawingViewGenerativeLinks1
DrawingView.Size View_Size_01
drawingView2.ReferenceView = DrawingView
drawingView2.AlignedWithReferenceView
drawingViewGenerativeBehavior2.Update
drawingView2.Size View_Size_02
'--------------------------------------------------------------------------------------------------- 거리값 생성
Right_Length01 = (Abs(View_Size_01(0) * Trigonometry_Form.View_Scale_Text) + Abs(View_Size_01(0)) * Trigonometry_Form.View_Scale_Text) / 3    ' 정면뷰의 1/3
Right_Length02 = (Abs(View_Size_02(0) * Trigonometry_Form.View_Scale_Text) + Abs(View_Size_02(0)) * Trigonometry_Form.View_Scale_Text) / 3 ' 우측뷰의 1/3
drawingView2.X = Abs(View_Size_01(1) * Trigonometry_Form.View_Scale_Text) + Abs(View_Size_02(0) * Trigonometry_Form.View_Scale_Text) + Right_Length01 + Right_Length02
drawingView2.Scale2 = Trigonometry_Form.View_Scale_Text
drawingView2.GenerativeBehavior.ForceUpdate
'--------------------------------------------------------------------------------------------------- Text 제거
Call View_Name_Text_Delete(drawingView2)
'---------------------------------------------------------------------------------------------------
'--------------------------------------------------------------------------------------------------- Top View 생성
'---------------------------------------------------------------------------------------------------
Set drawingViewGenerativeBehavior1 = DrawingView.GenerativeBehavior
Set drawingView2 = DrawingViews.Add("AutomaticNaming")
Set drawingViewGenerativeBehavior2 = drawingView2.GenerativeBehavior
drawingViewGenerativeBehavior2.DefineProjectionView drawingViewGenerativeBehavior1, 2
Set drawingViewGenerativeLinks1 = drawingView2.GenerativeLinks
Set drawingViewGenerativeLinks2 = DrawingView.GenerativeLinks
drawingViewGenerativeLinks2.CopyLinksTo drawingViewGenerativeLinks1
drawingView2.X = 0
drawingView2.Y = 10
drawingViewGenerativeBehavior2.Update
drawingView2.ReferenceView = DrawingView
drawingView2.AlignedWithReferenceView
Call Delay(1)
'--------------------------------------------------------------------------------------------------- 거리값 생성
drawingView2.X = 0
drawingView2.Size View_Size_02
Top_Length01 = (Abs(View_Size_01(2) * Trigonometry_Form.View_Scale_Text) + Abs(View_Size_01(3)) * Trigonometry_Form.View_Scale_Text) / 3   ' 정면뷰의 1/3
Top_Length02 = (Abs(View_Size_02(2) * Trigonometry_Form.View_Scale_Text) + Abs(View_Size_02(3)) * Trigonometry_Form.View_Scale_Text) / 3 ' 평면뷰의 1/3
drawingView2.Y = Abs(View_Size_01(3) * Trigonometry_Form.View_Scale_Text) + Abs(View_Size_02(2) * Trigonometry_Form.View_Scale_Text) + Top_Length01 + Top_Length02
drawingView2.Scale2 = Trigonometry_Form.View_Scale_Text
'--------------------------------------------------------------------------------------------------- Text 제거
Call View_Name_Text_Delete(drawingView2)
'--------------------------------------------------------------------------------------------------- Fit_All_In
Call Fit_All_In
Message_Label.Caption = "View 생성 완료"
End Sub
'---------------------------------------------------------------------------------------------------
' License Check                                                         &&&&&&&&&&&&&&&&&&&&&&&&&&&
'---------------------------------------------------------------------------------------------------
Private Function License_Check() As Variant
mdate = DateSerial(2015, 6, 30)
cha = DateDiff("d", Now, mdate)
    If cha < 0 Then
        aaa = MsgBox("LICENSE 사용기간이 지났습니다.", vbInformation, "ISPark CATIA Automation")
        End
    End If
End Function
